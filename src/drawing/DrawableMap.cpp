#include "DrawableMap.hpp"
#include "../map/Map.hpp"

namespace drawing
{

DrawableMap::BiomeCorner::
BiomeCorner(const Circle& shape, Biome biome) :
    shape{shape},
    biome{biome}
{

}

sf::Vertex makeInviVert(float x, float y)
{
    return sf::Vertex{ sf::Vector2f{x, y}, INVI };
}

sf::Vertex makeInviVert(const map::Point& p)
{
    return makeInviVert(p.x, p.y);
}

DrawableMap::
DrawableMap(const map::Map& map)
{
    sourceShapes.reserve(map.polygons.size());
    cornerShapes.reserve(map.corners.size());
    cellShapes.reserve(map.polygons.size());

    background.setFillColor(defaultBiomeColorMap().at(Biome::Ocean));
    background.setSize({ map.width(), map.height() });

    sf::CircleShape cs(1.0f);
    cs.setOrigin(cs.getRadius(), cs.getRadius());
    cs.setFillColor(INVI);

    for (int i = 0; i < (int)map.polygons.size(); ++i)
    {
        // Sources
        auto const& polygon = map.polygons[i];
        cs.setPosition(polygon.source.x, polygon.source.y);
        sourceShapes.push_back(cs);
        // Polygon biomes
        CellShape cell;
        cell.points.resize(2 + polygon.corners.size());
        cell.points[0] = makeInviVert(polygon.source);
        int cpid = 1;
        for (int cid : polygon.corners)
            cell.points[cpid++] = makeInviVert(map.corners[cid].position);
        cell.points[cpid] = cell.points[1];
        cell.biome = polygon.biome;
        cell.elevation = polygon.elevation;
        cell.moisture = polygon.moisture;
        auto  bounds = cell.points.getBounds();
        auto& biggestBounds = biggestPolygonBounds;
        biggestBounds.width  = std::max(biggestBounds.width,  bounds.width);
        biggestBounds.height = std::max(biggestBounds.height, bounds.height);
        cellShapes.push_back(cell);
        // Delaunay edges
        for (int j : polygon.neighbors) if (i < j)
            delauneyLines.emplace_back(Line{
                makeInviVert(map.polygons[i].source),
                makeInviVert(map.polygons[j].source)
            });
    }
    float riverStep = 3.0 / map.avgRiverLen;
    for (int i = 0; i < (int)map.corners.size(); ++i)
    {
        auto const& corner = map.corners[i];
        // Corner "biomes"
        Biome biome = Biome::Land;
        if      (corner.isBoundary) biome = Biome::Border;
        else if (corner.isCoast)    biome = Biome::Coast;
        else if (corner.isShore)    biome = Biome::Shore;
        else if (corner.isOcean)    biome = Biome::Ocean;
        else if (corner.isWater)    biome = Biome::Lake;
        else if (corner.river > 0)  biome = Biome::River;
        cs.setPosition(corner.position.x, corner.position.y);
        cornerShapes.emplace_back(cs, biome);
        // Voronoi edges
        for (int j : corner.neighbors) if (i < j)
        {
            voronoiLines.emplace_back(Line{
                makeInviVert(map.corners[i].position),
                makeInviVert(map.corners[j].position)
            });
            // shoreLines
            if (map.corners[i].isShore && map.corners[j].isShore)
                shoreLines.push_back(voronoiLines.back());
        }
        // Rivers
        if (corner.river == 1)
        {
            int a = i;
            while (!map.corners[a].isCoast)
            {
                int b = map.corners[a].watershed;

                auto const& pa = map.corners[a].position;
                auto const& pb = map.corners[b].position;

                sf::ConvexShape shape(4);
                float angle = pa.angleTo(pb);
                if (pb.x < pa.x) angle -= 180;
                float len = pa.distanceTo(pb);
                float r1 = riverStep*map.corners[a].river;
                float r2 = riverStep*map.corners[b].river;
                //lessen rivers joining effect
                if (r2 - r1 > 1) r2 = (r1 + r2) / 2;
                float thick1 = std::min(3.0f, r1);
                float thick2 = std::min(3.0f, r2);
                shape.setPoint(0, {0,  -thick1/2});
                shape.setPoint(1, {0,   thick1/2});
                shape.setPoint(2, {len, thick2/2});
                shape.setPoint(3, {len,-thick2/2});
                shape.setPosition(pa.x, pa.y);
                shape.rotate(angle);
                shape.setFillColor(INVI);
                watershedShapes.push_back(shape);

                sf::CircleShape circ(thick2/2);
                circ.setOrigin(circ.getRadius(), circ.getRadius());
                circ.setPosition(pb.x, pb.y);
                circ.setFillColor(INVI);
                riverLinkedNodes.push_back(circ);

                a = b;
            }
        }
    }
}

void DrawableMap::
draw(sf::RenderTarget& target, sf::RenderStates states)const
{
    target.draw(background);

    if (drawBiome)
        for (auto const& b : cellShapes)
            if (b.biome != Biome::Ocean && b.biome != Biome::Marsh)
                target.draw(b.points, states);
    if (drawRiver)
    {
        for (auto const& watershedShape : watershedShapes)
            target.draw(watershedShape, states);
        for (auto const& riverLinkedNode : riverLinkedNodes)
            target.draw(riverLinkedNode, states);
    }
    if (drawBiome)
        for (auto const& b : cellShapes)
            if (b.biome == Biome::Ocean || b.biome == Biome::Marsh)
                target.draw(b.points, states);

    if (drawShorelines)
        for (auto const& l : shoreLines)
            target.draw(&l[0], 2, sf::Lines);
    if (drawVoronoi)
        for (auto const& l : voronoiLines)
            target.draw(&l[0], 2, sf::Lines);
    if (drawDelaunay)
        for (auto const& l : delauneyLines)
            target.draw(&l[0], 2, sf::Lines);

    if (drawSource)
        for (auto const& s : sourceShapes)
            target.draw(s, states);
    if (drawCorner)
        for (auto const& b : cornerShapes)
            target.draw(b.shape, states);

    if (chosenPid != -1)
    {
        for (auto const& l : chosenPolygon)
            target.draw(&l[0], 2, sf::Lines);
        for (auto const& c : chosenPolygonCorners)
            target.draw(c, states);
    }
}

void DrawableMap::
setSourceDrawing(bool enable, float radius, sf::Color color)
{
    if (!(drawSource = enable)) return;
    for (auto& s : sourceShapes)
    {
        s.setRadius(radius);
        s.setOrigin(radius, radius);
        s.setFillColor(color);
    }
}

void DrawableMap::
setCornerDrawing(bool enable, float radius,
                 const std::map<Biome,sf::Color>& m)
{
    if (!(drawCorner = enable)) return;
    for (auto& b : cornerShapes)
    {
        if (m.count(b.biome))
            b.shape.setFillColor(m.at(b.biome));
        b.shape.setRadius(radius);
        b.shape.setOrigin(radius, radius);
    }
}

void DrawableMap::
setVoronoiDrawing(bool enable, sf::Color color)
{
    if (!(drawVoronoi = enable)) return;
    for (auto& l : voronoiLines) l[0].color = l[1].color = color;
}

void DrawableMap::
setDelaunayDrawing(bool enable, sf::Color color)
{
    if (!(drawDelaunay = enable)) return;
    for (auto& l : delauneyLines) l[0].color = l[1].color = color;
}

void DrawableMap::
setBiomeDrawing(bool enable, const std::map<Biome,sf::Color>& m)
{
    if (!(drawBiome = enable)) return;
    drawShorelines = false;
    if (m.count(Biome::Lake))
    {
        sf::Color lakeColor = m.at(Biome::Lake);
        for (auto& l : shoreLines) l[0].color = l[1].color = lakeColor;
    }
    for (auto& cell : cellShapes) if (m.count(cell.biome))
    {
        sf::Color color = m.at(cell.biome);
        for (auto v = cell.points.getVertexCount(); v--; )
            cell.points[v].color = color;
    }
}

void DrawableMap::
setRiverDrawing(bool enable, sf::Color color)
{
    if (!(drawRiver = enable)) return;
    for (auto& watershedShape : watershedShapes)
        watershedShape.setFillColor(color);
    for (auto& riverLinkedNode : riverLinkedNodes)
        riverLinkedNode.setFillColor(color);
}

sf::Color DrawableMap::
lerp(sf::Color a, sf::Color b, float s)
{   //return s*a + (1-s)*b
    float t = 1 - s;
    float rr = (float)a.r * s + (float)b.r * t;
    float gg = (float)a.g * s + (float)b.g * t;
    float bb = (float)a.b * s + (float)b.b * t;
    return sf::Color(rr, gg, bb);
}

void DrawableMap::
setElevationDrawing(sf::Color inlandColor)
{
    drawShorelines = true;
    for (auto& cell : cellShapes) if (cell.biome != Biome::Ocean)
    {
        sf::Color color = lerp(sf::Color::White, inlandColor, cell.elevation);
        for (auto v = cell.points.getVertexCount(); v--; )
            cell.points[v].color = color;
    }
}

void DrawableMap::
setMoistureDrawing(sf::Color inlandColor)
{
    drawShorelines = true;
    for (auto& cell : cellShapes) if (cell.biome != Biome::Ocean)
    {
        sf::Color color = lerp(inlandColor, sf::Color::White, cell.moisture);
        for (auto v = cell.points.getVertexCount(); v--; )
            cell.points[v].color = color;
    }
}

const DrawableMap::BiomeColorMap& DrawableMap::
defaultBiomeColorMap()
{
    using Biome = map::Biome;
    static BiomeColorMap biomeColorMap{             //179, 166, 146
        {Biome::Land,                       sf::Color{100, 150, 100}},
        {Biome::Water,                      sf::Color{0, 0, 255}},
        {Biome::Ocean,                      sf::Color{54, 54, 97}},
        {Biome::Lake,                       sf::Color{91, 132, 173}},
        {Biome::Ice,                        sf::Color{100, 255, 255}},
        {Biome::Marsh,                      sf::Color{133, 118, 59}},
        {Biome::Beach,                      sf::Color{172, 159, 139}},
        {Biome::Snow,                       sf::Color{248, 248, 248}},
        {Biome::Tundra,                     sf::Color{221, 221, 187}},
        {Biome::Bare,                       sf::Color{187, 187, 187}},
        {Biome::Scorched,                   sf::Color{153, 153, 153}},
        {Biome::Taiga,                      sf::Color{204, 212, 187}},
        {Biome::Shrubland,                  sf::Color{196, 204, 187}},
        {Biome::TemperateDesert,            sf::Color{228, 232, 202}},
        {Biome::TemperateRainForest,        sf::Color{164, 196, 168}},
        {Biome::TemperateDeciduousForest,   sf::Color{180, 201, 169}},
        {Biome::Grassland,                  sf::Color{196, 212, 170}},
        {Biome::TropicalRainForest,         sf::Color{156, 187, 169}},
        {Biome::TropicalSeasonalForest,     sf::Color{169, 204, 164}},
        {Biome::SubtropicalDesert,          sf::Color{233, 221, 199}},
    };
    return biomeColorMap;
}

const DrawableMap::BiomeColorMap& DrawableMap::
defaultCornerColorMap()
{
    using Biome = map::Biome;
    static BiomeColorMap cornerColorMap{
        {Biome::Border, sf::Color{255, 0, 0, 100}},
        {Biome::Land,   sf::Color{0, 0, 0, 100}},
        {Biome::Coast,  sf::Color{255, 255, 0, 100}},
        {Biome::Shore,  sf::Color{255, 0, 255, 100}},
        {Biome::Ocean,  sf::Color{0, 0, 255, 100}},
        {Biome::Lake,   sf::Color{0, 255, 255, 100}},
        {Biome::River,  sf::Color{255, 255, 255, 100}},
    };
    return cornerColorMap;
}

std::vector<sf::Color> getCornerColors(int n)
{
    // Making annoying rainbows in javascript (Jim Bumgardner)
    // https://krazydad.com/tutorials/makecolors.php
    std::vector<sf::Color> ret;
    float phase = 0;
    float center = 128;
    float width = 127;
    float frequency = M_PI*2 / n;
    for (int i = 0; i < n; ++i)
    {
        unsigned char red   = sin(frequency*i+2+phase) * width + center;
        unsigned char green = sin(frequency*i+0+phase) * width + center;
        unsigned char blue  = sin(frequency*i+4+phase) * width + center;
        ret.emplace_back(red, green, blue);
    }
    return ret;
}

void DrawableMap::
setChosenPid(int pid)
{
    if (chosenPid == pid) return;
    chosenPid = pid;
    if (chosenPid == -1) return;

    chosenPolygon.clear();
    chosenPolygonCorners.clear();
    int nCorners = cellShapes[pid].points.getVertexCount() - 2;
    auto colors = getCornerColors(nCorners);
    float radius = 0.04f * std::max(biggestPolygonBounds.width,
                                    biggestPolygonBounds.height);
    Circle circ(radius);
    circ.setOrigin(radius, radius);
    for (int i = 1; i <= nCorners; ++i)
    {
        Line line;
        line[0] = cellShapes[pid].points[i];
        line[1] = cellShapes[pid].points[i+1];
        line[0].color = line[1].color = sf::Color::Black;
        chosenPolygon.push_back(line);

        circ.setPosition(line[0].position);
        circ.setFillColor(colors[i-1]);
        chosenPolygonCorners.push_back(circ);
    }
}

const sf::FloatRect& DrawableMap::
getBiggestPolygonBounds()const
{
    return biggestPolygonBounds;
}

const sf::VertexArray& DrawableMap::
getDisplayPolygon(int pid)const
{
    return cellShapes[pid].points;
}

const DrawableMap::CellShape& DrawableMap::
getCellShape(int pid)const
{
    return cellShapes[pid];
}

} //namespace drawing
