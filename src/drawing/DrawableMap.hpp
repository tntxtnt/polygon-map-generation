#ifndef DRAWING_DRAWABLEMAP_HPP
#define DRAWING_DRAWABLEMAP_HPP

#define _USE_MATH_DEFINES
#include <cmath>
#include <vector>
#include <array>
#include <map>
#include <SFML/Graphics.hpp>
#include "../map/Biome.hpp"

namespace map
{
class Map;
}

const sf::Color INVI = sf::Color::Transparent;

namespace drawing
{

class DrawableMap : public sf::Drawable
{
    using Biome = map::Biome;
public:
    using Line         = std::array<sf::Vertex, 2>;
    using LineVec      = std::vector<Line>;
    using LineVecVec   = std::vector<LineVec>;
    using Circle       = sf::CircleShape;
    using CircleVec    = std::vector<Circle>;
    struct BiomeCorner {
        Circle shape;
        Biome  biome = Biome::Land;
        BiomeCorner(const Circle& shape, Biome biome);
    };
    using BiomeCornerVec = std::vector<BiomeCorner>;
    using BiomeColorMap = std::map<Biome, sf::Color>;
    using ConvexVec = std::vector<sf::ConvexShape>;
    struct CellShape {
        sf::VertexArray points      {sf::TriangleFan, 0};
        Biome           biome     = Biome::Land;
        double          elevation = 0;
        double          moisture  = 0;
    };
    using CellShapeVec = std::vector<CellShape>;
public:
    static const BiomeColorMap& defaultBiomeColorMap();
    static const BiomeColorMap& defaultCornerColorMap();
public:
    DrawableMap(const map::Map& map);
    void setSourceDrawing(bool enable, float rad=0, sf::Color color=INVI);
    void setCornerDrawing(bool enable, float rad=0, const BiomeColorMap& m={});
    void setVoronoiDrawing(bool enable, sf::Color color=INVI);
    void setDelaunayDrawing(bool enable, sf::Color color=INVI);
    void setBiomeDrawing(bool enable, const BiomeColorMap& m={});
    void setRiverDrawing(bool enable, sf::Color color=INVI);
    void setElevationDrawing(sf::Color color=INVI);
    void setMoistureDrawing(sf::Color color=INVI);
    void setChosenPid(int pid);
    const sf::FloatRect&   getBiggestPolygonBounds()const;
    const sf::VertexArray& getDisplayPolygon(int pid)const;
    const CellShape& getCellShape(int pid)const;
private:
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states)const;
    static sf::Color lerp(sf::Color a, sf::Color b, float s);
private:
    bool            drawSource     = false;
    bool            drawCorner     = false;
    bool            drawVoronoi    = false;
    bool            drawDelaunay   = false;
    bool            drawBiome      = false;
    bool            drawRiver      = false;
    bool            drawShorelines = false;
    CircleVec       sourceShapes{};
    BiomeCornerVec  cornerShapes{};
    LineVec         voronoiLines{};
    LineVec         delauneyLines{};
    LineVec         shoreLines{};
    ConvexVec       watershedShapes{};
    CircleVec       riverLinkedNodes{};
    CellShapeVec    cellShapes{};
    int             chosenPid = -1;
    LineVec         chosenPolygon{};
    CircleVec       chosenPolygonCorners{};
    sf::FloatRect   biggestPolygonBounds{0, 0, 0, 0};
    sf::RectangleShape background;
};

} //namespace drawing

#endif // DRAWING_DRAWABLEMAP_HPP
