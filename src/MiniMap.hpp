#ifndef MINIMAP_HPP
#define MINIMAP_HPP

#include "Navigator.hpp"

class MiniMap : public Navigator
{
public:
    using Navigator::Navigator;
    void                      setViewBorder(float thickness, sf::Color color);
    void                      setBigMapViewRect(const sf::FloatRect* pRect);
    const sf::RectangleShape& getSyncViewBorder();
private:
    float                borderThickness = 6.0f;
    const sf::FloatRect* pBigMapViewRect = nullptr;
    sf::RectangleShape   viewBorder{};
};

#endif // MINIMAP_HPP
