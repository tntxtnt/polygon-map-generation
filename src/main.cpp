#include <ctime>
#include <fmt/format.h>
#include "map/shape/island/Perlin.hpp"
#include "map/poly/ImprovedVoroGen.hpp"
#include "map/Map.hpp"
#include "drawing/DrawableMap.hpp"
#include "Navigator.hpp"
#include "MiniMap.hpp"
#include "PolygonPicker.hpp"
#include <array>
#include <thread>
#include "imgui/imgui.h"
#include "imgui/imgui-SFML.h"

void saveMapImage(const sf::RenderWindow& window, const std::string& fname,
                  const sf::IntRect& mapArea);
drawing::DrawableMap getDrawableMap(const map::Map& map);
void enableDrawElevations(drawing::DrawableMap& drawableMap);
void enableDrawMoisture(drawing::DrawableMap& drawableMap);
void enableDrawBiomes(drawing::DrawableMap& drawableMap);
void screenshot(const drawing::DrawableMap& drawableMap, std::string filename,
                unsigned imgW, unsigned imgH, float mapW, float mapH, sf::Text& status);
void printControlsPanel(float x, float y, float w, float h);
void printInformationPanel(float x, float y, float w, float h, int npoly1, int npoly2,
                           int niter, int seed1, int seed2, int seed3, 
                           int riverPick, float zoomPerc);
int normalizeIntRange(int n, int min, int max);

int main()
{
    unsigned windowW        = 1080;
    unsigned windowH        = 620;
    float    mapW           = 600;
    float    mapH           = 600;
    unsigned pointCount     = 10000;
    unsigned relaxCount     = 2;
    unsigned riverPickCount = 100;

    sf::ContextSettings contextSettings;
    contextSettings.antialiasingLevel = 8;
    sf::RenderWindow window(sf::VideoMode(windowW, windowH),
        "Polygonal Map Generation", sf::Style::Titlebar | sf::Style::Close,
        contextSettings);
    window.setFramerateLimit(30);
    ImGui::SFML::Init(window);

    sf::Font font;
    font.loadFromFile("font/Cousine.ttf");

    //std::string sseed = "Hello";
    //std::getline(std::cin, sseed);
    //seed = std::hash<std::string>{}(sseed);
    //1501881711: bottom left corner on boundary
    //1501954395: huge lake
    unsigned seed = time(nullptr);
    unsigned polySeed  = seed;
    unsigned shapeSeed = seed;
    unsigned riverSeed = seed;
    // Poly map generator
    map::poly::ImprovedVoroGen voronoiGen(polySeed, pointCount, mapW, mapH, relaxCount);
    // Map shape generator
    map::shape::island::Perlin islandPerlinShape;
    islandPerlinShape.seed(shapeSeed);
    islandPerlinShape.loadSettings("islandPerlinSettings.txt");
    islandPerlinShape.setMapSize(mapW, mapH);
    // Map and drawable/display map
    map::Map map(&voronoiGen, &islandPerlinShape, riverSeed, riverPickCount);
    auto drawableMap = getDrawableMap(map);
    // Map navigator
    float mapNavW = 600;
    float mapNavH = 600;
    float mapNavX = (windowW - mapNavW) / 2.0f + 20; //magic 20px offset x
    float mapNavY = (windowH - mapNavH) / 2.0f;
    Navigator mapNav{{0, 0, mapW, mapH}, {mapNavX, mapNavY, mapNavW, mapNavH},
                     window.getSize()};
    // Minimap
    float minimapSize = 200;
    float minimapX = mapNavX + mapNavW + mapNavY;
    float minimapY = mapNavY;
    MiniMap minimap{{0, 0, mapW, mapH},
                    {minimapX, minimapY, minimapSize, minimapSize},
                    window.getSize()};
    minimap.setViewBorder(6.0f, sf::Color::Red);
    minimap.setBigMapViewRect(&mapNav.getCapturedRect());
    // Polygon picker
    auto polyPickerCellSize = drawableMap.getBiggestPolygonBounds();
    unsigned nRows = mapH / polyPickerCellSize.height;
    unsigned nCols = mapNavW / polyPickerCellSize.width;
    PolygonPicker polyPicker{nRows, nCols, mapNavX, mapNavY, mapNavW, mapH};
    polyPicker.parse(map);
    // F11 status
    sf::Text status;
    status.setFont(font);
    status.setCharacterSize(12);
    status.setFillColor(sf::Color::Red);
    status.setPosition(mapNavX + 4, windowH - mapNavY - 18);
    // Cell view rect
    int chosenPid = -1;
    float cellNavSize = 100;
    float cellNavX = mapNavX + mapNavW + mapNavY + (minimapSize - cellNavSize)/2;
    float cellNavY = mapNavY*2 + minimapSize + 50;
    float cellViewSize = std::max(polyPickerCellSize.width, polyPickerCellSize.height);
    Navigator cellNav{{0, 0, cellViewSize, cellViewSize},
                      {cellNavX, cellNavY, cellNavSize, cellNavSize},
                      window.getSize()};
    // Cell labels
    std::array<sf::Text, 3> cellLabels;
    for (size_t i = 0; i < cellLabels.size(); ++i)
    {
        cellLabels[i].setFont(font);
        cellLabels[i].setCharacterSize(12);
        cellLabels[i].setFillColor(sf::Color::White);
        cellLabels[i].setPosition(cellNavX-5, cellNavY+cellNavSize+10+i*18);
    }
    // UI, temporary input variables
    float leftPanelW = 250;
    float panel1H = 150;
    float panel3H = 135;
    float panel2H = windowH - panel1H - panel3H - 10;
    int inputPointCount = pointCount;
    int inputSeedPoly = seed;
    int inputSeedShape = seed;
    int inputSeedRiver = seed;
    int inputRiverPickCount = riverPickCount;
    int maxRiverPickCount = 200;
    int mapDisplayMethod = 3;
    int inputRelaxCount = relaxCount;
    int maxRelaxCount = 50;
    bool needMapRebuild = false;

    sf::Clock deltaClock;
    while (window.isOpen())
    {
        sf::Event e;
        while (window.pollEvent(e))
        {
            ImGui::SFML::ProcessEvent(e);
            if (e.type == sf::Event::Closed)
                window.close();
            else if (e.type == sf::Event::KeyPressed)
            {
                if (e.key.code == sf::Keyboard::F11)
                {
                    drawableMap.setChosenPid(chosenPid = -1);
                    enableDrawBiomes(drawableMap);
                    std::thread sst(screenshot, std::cref(drawableMap),
                        fmt::format("screenshot/{}-{}-{}-{}-{}-{}.png", pointCount,
                            polySeed, shapeSeed, riverSeed, riverPickCount, relaxCount),
                        2048, 2048, mapW, mapH, std::ref(status));
                    sst.detach();
                }
                else if (e.key.code == sf::Keyboard::Escape)
                    drawableMap.setChosenPid(chosenPid = -1);
            }
            else if (e.type == sf::Event::MouseButtonPressed)
            {
                auto const& mbt = e.mouseButton;
                if (mbt.button == sf::Mouse::Right)
                {
                    if (!mapNav.isInsideEventRect(mbt.x, mbt.y))
                    {
                        drawableMap.setChosenPid(chosenPid = -1);
                        continue;
                    }
                    //account for mapNav transformation
                    auto t = mapNav.getTransformedMousePos(mbt.x, mbt.y);
                    chosenPid = polyPicker.getPickedPid(t.x, t.y, map);
                    drawableMap.setChosenPid(chosenPid);
                    if (chosenPid == -1) continue;
                    auto bounds = drawableMap.getDisplayPolygon(chosenPid).getBounds();
                    float bx = bounds.left + bounds.width / 2;
                    float by = bounds.top + bounds.height / 2;
                    sf::FloatRect viewRect{bx - cellViewSize/2, by - cellViewSize/2,
                                           cellViewSize, cellViewSize};
                    cellNav.setSourceRect(viewRect);
                    auto const& cellShape = drawableMap.getCellShape(chosenPid);
                    cellLabels[0].setString(fmt::format("{}", cellShape.biome));
                    cellLabels[1].setString(fmt::format("Elevation: {:.3f}", cellShape.elevation));
                    cellLabels[2].setString(fmt::format(" Moisture: {:.3f}", cellShape.moisture));
                    auto biomeTextRect = cellLabels[0].getGlobalBounds();
                    int dx = (minimapSize - biomeTextRect.width) / 2;
                    cellLabels[0].setPosition(minimapX + dx, cellNavY + cellNavSize + 10);
                }
            }
            mapNav.handleEvent(e);
        }

        // ImGui
        auto timeDelta = deltaClock.restart();
        ImGui::SFML::Update(window, timeDelta);
        printInformationPanel(0, 0, leftPanelW, panel1H, map.polygons.size(), pointCount,
            voronoiGen.getNumRelaxations(), polySeed, shapeSeed, riverSeed,
            riverPickCount, 100 * mapNav.getZoomPercentage());
        printControlsPanel(0, windowH - panel3H, leftPanelW, panel3H);
        //printRandomizerPanel
        ImGui::Begin("Randomizer");
        ImGui::SetWindowPos(ImVec2(0, panel1H + 5));
        ImGui::SetWindowSize(ImVec2(leftPanelW, panel2H));
        if (ImGui::InputInt("Shape", &inputSeedShape))
            needMapRebuild = inputSeedShape != shapeSeed;
        ImGui::Separator();

        if (ImGui::Button("500"))
            needMapRebuild = (inputPointCount = 500) != pointCount;
        ImGui::SameLine();
        if (ImGui::Button("1k"))
            needMapRebuild = (inputPointCount = 1000) != pointCount;
        ImGui::SameLine();
        if (ImGui::Button("2k"))
            needMapRebuild = (inputPointCount = 2000) != pointCount;
        ImGui::SameLine();
        if (ImGui::Button("5k"))
            needMapRebuild = (inputPointCount = 5000) != pointCount;
        ImGui::SameLine();
        if (ImGui::Button("10k"))
            needMapRebuild = (inputPointCount = 10000) != pointCount;
        ImGui::SameLine();
        if (ImGui::Button("20k"))
            needMapRebuild = (inputPointCount = 20000) != pointCount;
        ImGui::SameLine();
        ImGui::Text("Points");
        if (ImGui::InputInt("Polygon", &inputSeedPoly))
            needMapRebuild = inputSeedPoly != polySeed;
        if (ImGui::InputInt("Relax", &inputRelaxCount, 1, 5))
        {
            inputRelaxCount = normalizeIntRange(inputRelaxCount, 0, maxRelaxCount);
            needMapRebuild = inputRelaxCount != relaxCount;
        }
        if (ImGui::Button("Relax++") && relaxCount < maxRelaxCount)
        {
            inputRelaxCount = ++relaxCount;
            voronoiGen.relax();
            map.rebuild();
            drawableMap = getDrawableMap(map);
            polyPicker.parse(map);
            drawableMap.setChosenPid(chosenPid = -1);
        }
        ImGui::Separator();

        if (ImGui::InputInt("River", &inputSeedRiver))
            needMapRebuild = inputSeedRiver != riverSeed;
        if (ImGui::InputInt("R.Density", &inputRiverPickCount, 10, 100))
        {
            inputRiverPickCount = normalizeIntRange(inputRiverPickCount, 0, maxRiverPickCount);
            needMapRebuild = inputRiverPickCount != riverPickCount;
        }
        ImGui::Separator();

        if (ImGui::RadioButton("Elevation", &mapDisplayMethod, 1))
            enableDrawElevations(drawableMap);
        if (ImGui::RadioButton("Moisture", &mapDisplayMethod, 2))
            enableDrawMoisture(drawableMap);
        if (ImGui::RadioButton("Biome", &mapDisplayMethod, 3))
            enableDrawBiomes(drawableMap);
        ImGui::End();

        // Rebuild map if needed
        if (needMapRebuild)
        {
            needMapRebuild = false;

            pointCount = inputPointCount;
            shapeSeed = inputSeedShape;
            polySeed = inputSeedPoly;
            riverSeed = inputSeedRiver;
            riverPickCount = inputRiverPickCount;
            relaxCount = inputRelaxCount;

            voronoiGen = map::poly::ImprovedVoroGen(
                polySeed, pointCount, mapW, mapH, relaxCount);
            islandPerlinShape.seed(shapeSeed);
            map = map::Map(&voronoiGen, &islandPerlinShape, riverSeed, riverPickCount);
            map.rebuild();
            drawableMap = getDrawableMap(map);
            polyPicker.parse(map);
            drawableMap.setChosenPid(chosenPid = -1);
        }

        // ==== DRAWINGS ====
        window.clear();
        // Draw big map
        window.setView(mapNav.getView());
        window.draw(drawableMap);
        // Draw mini map
        window.setView(minimap.getView());
        window.draw(drawableMap);
        window.draw(minimap.getSyncViewBorder());
        // Draw chosen cell
        if (chosenPid != -1)
        {
            window.setView(cellNav.getView());
            window.draw(drawableMap);
            window.setView(window.getDefaultView());
            for (auto const& label : cellLabels) window.draw(label);
        }
        // Draw imgui
        window.setView(window.getDefaultView());
        ImGui::SFML::Render(window);
        // Draw F11 status
        window.draw(status); 

        window.display();
    }
}

void enableDrawElevations(drawing::DrawableMap& drawableMap)
{
    drawableMap.setElevationDrawing(sf::Color{50, 100, 50});
}

void enableDrawMoisture(drawing::DrawableMap& drawableMap)
{
    drawableMap.setMoistureDrawing(sf::Color::Blue);
}

void enableDrawBiomes(drawing::DrawableMap& drawableMap)
{
    drawableMap.setBiomeDrawing(true,
        drawing::DrawableMap::defaultBiomeColorMap());
}

drawing::DrawableMap getDrawableMap(const map::Map& map)
{
    drawing::DrawableMap drawableMap(map);
    drawableMap.setRiverDrawing(true, sf::Color{91, 132, 173});
    enableDrawBiomes(drawableMap);

    //drawableMap.setSourceDrawing(true, 1.0f, sf::Color::Black);
    //drawableMap.setVoronoiDrawing(true, sf::Color{0, 0, 0, 40});
    //drawableMap.setDelaunayDrawing(true, sf::Color::Red);
    //drawableMap.setCornerDrawing(true, 0.5f,
    //    drawing::DrawableMap::defaultCornerColorMap());

    return drawableMap;
}

void saveMapImage(const sf::RenderWindow& window, const std::string& fname,
                  const sf::IntRect& mapArea)
{
    sf::Vector2u windowSize = window.getSize();
    sf::Texture texture;
    texture.create(windowSize.x, windowSize.y);
    texture.update(window);
    sf::Image screenshot = texture.copyToImage();
    sf::Image mapImg;
    mapImg.create(600, 600);
    mapImg.copy(screenshot, 0, 0, mapArea);
    mapImg.saveToFile(fname);
}

void screenshot(const drawing::DrawableMap& drawableMap, std::string filename,
                unsigned imgW, unsigned imgH, float mapW, float mapH, sf::Text& status)
{
    sf::ContextSettings context;
    context.antialiasingLevel = 8;
    sf::RenderWindow window(sf::VideoMode(imgW, imgH), "Screenshot",
                            sf::Style::None, context);
    Navigator mapNav{{0, 0, mapW, mapH},
                     {0, 0, (float)imgW, (float)imgH},
                     window.getSize()};
    window.clear();
    window.setView(mapNav.getView());
    window.draw(drawableMap);
    sf::Vector2u windowSize = window.getSize();
    sf::Texture texture;
    texture.create(windowSize.x, windowSize.y);
    texture.update(window);
    status.setString("Saving...");
    texture.copyToImage().saveToFile(filename);
    window.close();
    status.setString("");
}

void printControlsPanel(float x, float y, float w, float h)
{
    ImGui::Begin("Controls");
    ImGui::Text("Right click - Pick cell");
    ImGui::Text("Esc         - Unpick cell");
    ImGui::Text("Drag        - Move map");
    ImGui::Text("Scroll      - Zoom");
    ImGui::Text("Mid click   - Restore zoom");
    ImGui::Text("F11 - Export PNG (2048x2048)");
    ImGui::SetWindowPos(ImVec2(x, y));
    ImGui::SetWindowSize(ImVec2(w, h));
    ImGui::End();
}

void printInformationPanel(float x, float y, float w, float h, int npoly1, int npoly2,
                           int niter, int seed1, int seed2, int seed3, int riverPick, 
                           float zoomPerc)
{
    ImGui::Begin("Information");
    ImGui::Text("Polygons: %d (%d)", npoly1, npoly2);
    ImGui::Text("Relax iterations: %d", niter);
    ImGui::Text("Seeds: %d (poly)", seed1);
    ImGui::Text("       %d (shape)", seed2);
    ImGui::Text("       %d (river)", seed3);
    ImGui::Text("River density: %d", riverPick);
    ImGui::Separator();
    ImGui::Text("Zoom: %.0f%%", zoomPerc);
    ImGui::SetWindowPos(ImVec2(x, y));
    ImGui::SetWindowSize(ImVec2(w, h));
    ImGui::End();
}

int normalizeIntRange(int n, int min, int max)
{
    n = std::min(n, max);
    n = std::max(n, min);
    return n;
}