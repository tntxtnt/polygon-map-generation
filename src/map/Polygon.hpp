#ifndef MAP_POLYGON_HPP
#define MAP_POLYGON_HPP

#include "Point.hpp"
#include "Biome.hpp"
#include <vector>
using IntVec = std::vector<int>;

namespace map
{

struct Polygon {
    Point  source{};
    IntVec neighbors{}; //Delaunay triangulation's adjacent list
    IntVec corners{};   //Actually it is corner indices
    bool   isBoundary = false;

    bool   isWater    = false;
    bool   isOcean    = false;
    bool   isCoast    = false;
    double elevation  = 0;
    double moisture   = 0;
    Biome  biome      = Biome::Land;

    void clear();
};

using PolygonVec = std::vector<Polygon>;

}

#endif // MAP_POLYGON_HPP
