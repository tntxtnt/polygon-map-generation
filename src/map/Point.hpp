//Point definition

#ifndef MAP_POINT_HPP
#define MAP_POINT_HPP

#define _USE_MATH_DEFINES
#include <cmath>
#include <vector>

namespace map
{

struct Point {
    float x, y;

    Point();
    Point(float x, float y);
    bool operator<(const Point& rhs)const;
    bool operator<=(const Point& rhs)const;
    bool operator>(const Point& rhs)const;
    bool operator>=(const Point& rhs)const;
    bool operator==(const Point& rhs)const;
    bool operator!=(const Point& rhs)const;

    const Point& operator+=(const Point& rhs);
    const Point& operator-=(const Point& rhs);
    const Point& operator*=(const Point& rhs);
    const Point& operator/=(const Point& rhs);

    Point operator+(const Point& rhs)const;
    Point operator-(const Point& rhs)const;
    Point operator*(const Point& rhs)const;
    Point operator/(const Point& rhs)const;
    Point operator-()const;

    const Point& operator*=(float rhs);
    const Point& operator/=(float rhs);

    Point operator*(float rhs)const;
    Point operator/(float rhs)const;

    float angleTo(const Point& b)const;
    float distanceTo(const Point& b)const;
};

Point operator*(float lhs, const Point& rhs);

using PointVec = std::vector<Point>;

}

#endif // MAP_POINT_HPP
