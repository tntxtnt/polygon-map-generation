#ifndef MAP_POLY_IMPROVEDVOROGEN_HPP
#define MAP_POLY_IMPROVEDVOROGEN_HPP

#include "VoronoiGen.hpp"

namespace map::poly
{

class ImprovedVoroGen : public VoronoiGen
{
public:
    ImprovedVoroGen(unsigned seed, unsigned nPoints, float w, float h,
                    unsigned nRelaxations);
    virtual std::tuple<PolygonVec, CornerVec> generate()const;
    virtual ~ImprovedVoroGen();
private:
    // Voronoi graph can have very short edges (length < 1 pixel)
    //We can lengthen short edges but we will lose Voronoi properies,
    //by moving corner to the average of polygon sources it touches.
    void improveCorners(const PolygonVec& polygons, CornerVec& corners)const;
};

}

#endif // MAP_POLY_IMPROVEDVOROGEN_HPP
