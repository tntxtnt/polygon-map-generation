#include "ImprovedVoroGen.hpp"

namespace map::poly
{

ImprovedVoroGen::
ImprovedVoroGen(unsigned seed, unsigned nPoints, float w, float h,
                unsigned nRelaxations) :
    VoronoiGen{seed, nPoints, w, h, nRelaxations}
{

}

std::tuple<PolygonVec, CornerVec> ImprovedVoroGen::
generate()const
{
    auto [polygons, corners] = VoronoiGen::generate();
    improveCorners(polygons, corners);
    return {polygons, corners};
}

void ImprovedVoroGen::
improveCorners(const PolygonVec& polygons, CornerVec& corners)const
{
    for (Corner& corner : corners) if (!corner.isBoundary)
    {
        Point newPos{0, 0};
        for (int pid : corner.polygons)
            newPos += polygons[pid].source;
        newPos /= (float)corner.polygons.size();
        corner.position = newPos;
    }
}

ImprovedVoroGen::
~ImprovedVoroGen()
{

}

}
