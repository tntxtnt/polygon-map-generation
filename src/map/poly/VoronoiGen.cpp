#include "VoronoiGen.hpp"

namespace map::poly
{

VoronoiGen::
VoronoiGen(unsigned seed, unsigned nPoints, float w, float h,
           unsigned nRelaxations) :
    Generator{seed, nPoints, w, h},
    srcGen{seed, {w, h}, nPoints, nRelaxations}
{

}

std::tuple<PolygonVec, CornerVec> VoronoiGen::
generate()const
{
    PolygonVec polygons;
    CornerVec corners;

    const PointVec& source = srcGen.getSource();
    VoronoiDiagram vd;
    construct_voronoi(begin(source), end(source), &vd);

    std::vector<bool>    borderSource(source.size(), false);
    std::map<Point,bool> borderCorner;
    for (auto pCell = begin(vd.cells()); pCell != end(vd.cells()); ++pCell)
    {
        // collect corners and determine if this cell is border cell
        int sid = pCell->source_index();
        PointVec allCornerPos;
        const auto* pEdge = pCell->incident_edge();
        do {
            if (!collectCornerPositions(allCornerPos, pEdge->vertex0()))
                borderSource[sid] = true;
            if (!collectCornerPositions(allCornerPos, pEdge->vertex1()))
                borderSource[sid] = true;
            pEdge = pEdge->next();
        } while (pEdge != pCell->incident_edge());
        // assign corresponding type to its corners. If a corner is already
        // at border, don't change it
        for (const Point& cornerPos : allCornerPos)
            borderCorner[cornerPos] |= borderSource[sid];
    }

    // Build corners position and isBoundary
    for (auto& kv : borderCorner)
    {
        Corner corner;
        corner.position = kv.first;
        corner.isBoundary = kv.second;
        corners.push_back(corner);
    }
    //no need to sort corner by position since
    //corner's positions are already sorted in std::map
    // Build polygons position
    for (int i = 0; i < (int)borderSource.size(); ++i)
    {
        if (borderSource[i]) continue;
        Polygon polygon;
        polygon.source = source[i];
        polygons.push_back(polygon);
    }
    std::sort(begin(polygons), end(polygons), //sort polygon by source
        [](auto const& lhs, auto const& rhs){
            return lhs.source < rhs.source;
        });
    // Build corners neighbors and polygons
    // Build polygons
    for (auto pCell = begin(vd.cells()); pCell != end(vd.cells()); ++pCell)
    {
        int sid = pCell->source_index();
        if (borderSource[sid]) continue; //ignore border polygons
        int pid = getPolygonIndexBySource(polygons, source[sid]);
        Polygon& polygon = polygons[pid];

        const auto* pEdge = pCell->incident_edge();
        do {
            Point v1(pEdge->vertex0()->x(), pEdge->vertex0()->y());
            Point v2(pEdge->vertex1()->x(), pEdge->vertex1()->y());
            int cid1 = getCornerIndexByPosition(corners, v1);
            int cid2 = getCornerIndexByPosition(corners, v2);
            corners[cid1].neighbors.push_back(cid2);
            corners[cid2].neighbors.push_back(cid1);
            corners[cid1].polygons.push_back(pid);
            corners[cid2].polygons.push_back(pid);

            if (corners[cid1].isBoundary || corners[cid2].isBoundary)
                polygon.isBoundary = true;
            int twinSid = pEdge->twin()->cell()->source_index();
            if (!borderSource[twinSid])
                polygon.neighbors.push_back(
                    getPolygonIndexBySource(polygons, source[twinSid]));
            polygon.corners.push_back(cid1);

            pEdge = pEdge->next();
        } while (pEdge != pCell->incident_edge());
    }

    return {polygons, corners};
}

bool VoronoiGen::
collectCornerPositions(PointVec& cornersPos, const VoronoiVert* pVertex)const
{
    if (pVertex && !srcGen.isOutside(Point(pVertex->x(), pVertex->y())))
    {
        cornersPos.emplace_back(pVertex->x(), pVertex->y());
        return true;
    }
    return false;
}

int VoronoiGen::
getPolygonIndexBySource(const PolygonVec& polygons, const Point& src)const
{
    auto it = std::lower_bound(begin(polygons), end(polygons), Polygon(),
        [&src](auto const& lhs, auto const& rhs){
            return lhs.source < src;
        });
    return std::distance(begin(polygons), it);
}

int VoronoiGen::
getCornerIndexByPosition(const CornerVec& corners, const Point& pos)const
{
    auto it = std::lower_bound(begin(corners), end(corners), Corner(),
        [&pos](auto const& lhs, auto const& rhs){
            return lhs.position < pos;
        });
    return std::distance(begin(corners), it);
}

void VoronoiGen::
relax()
{
    srcGen.relax();
}

VoronoiGen::
~VoronoiGen()
{

}

unsigned VoronoiGen::
getNumPoints()const
{
    return srcGen.getSource().size();
}

unsigned VoronoiGen::
getNumRelaxations()const
{
    return srcGen.getRelaxCount();
}

}
