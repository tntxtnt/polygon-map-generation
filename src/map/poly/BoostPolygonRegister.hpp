//Register sf::Vector2f with Boost.Polygon

#ifndef MAP_POLY_BOOSTPOLYGONREGISTER_HPP
#define MAP_POLY_BOOSTPOLYGONREGISTER_HPP

#include "../Point.hpp"
#include <boost/polygon/voronoi.hpp>

namespace boost::polygon
{
template <>
struct geometry_concept<map::Point> {
    typedef point_concept type;
};
template <>
struct point_traits<map::Point> {
    typedef float coordinate_type;
    static inline coordinate_type get(const map::Point& p, orientation_2d o)
    { return (o == HORIZONTAL) ? p.x : p.y; }
};
}

namespace map::poly
{

using VoronoiDiagram = boost::polygon::voronoi_diagram<double>;
using VoronoiCell = VoronoiDiagram::cell_type;
using VoronoiEdge = VoronoiDiagram::edge_type;
using VoronoiVert = VoronoiDiagram::vertex_type;

}

#endif // MAP_POLY_BOOSTPOLYGONREGISTER_HPP
