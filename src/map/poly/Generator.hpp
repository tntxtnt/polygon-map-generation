#ifndef MAP_POLY_GENERATOR_HPP
#define MAP_POLY_GENERATOR_HPP

#include <tuple>
#include "../Polygon.hpp"
#include "../Corner.hpp"

namespace map::poly
{

class Generator
{
public:
    Generator(unsigned seed, unsigned nPoints, float w, float h);
    virtual std::tuple<PolygonVec, CornerVec> generate()const = 0;
    virtual ~Generator() = 0;

    unsigned getSeed()const;
    float    getBoundaryWidth()const;
    float    getBoundaryHeight()const;
    virtual unsigned getNumPoints()const;
protected:
    unsigned seed;
    unsigned nPoints;
    float    w;
    float    h;
};

}

#endif // MAP_POLY_GENERATOR_HPP
