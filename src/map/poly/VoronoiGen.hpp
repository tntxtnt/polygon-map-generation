#ifndef MAP_POLY_VORONOIGEN_HPP
#define MAP_POLY_VORONOIGEN_HPP

#include "Generator.hpp"
#include "VoronoiSourceGen.hpp"
#include <map>
#include <vector>
#include <algorithm>

namespace map::poly
{

class VoronoiGen : public Generator
{
public:
    VoronoiGen(unsigned seed, unsigned nPoints, float w, float h,
               unsigned nRelaxations);
    virtual std::tuple<PolygonVec, CornerVec> generate()const;
    void relax();
    virtual ~VoronoiGen();
    virtual unsigned getNumPoints()const;
    unsigned getNumRelaxations()const;
private:
    bool collectCornerPositions(PointVec& cornersPos,
                                const VoronoiVert* pVertex)const;
    int  getPolygonIndexBySource(const PolygonVec& polygons,
                                 const Point& src)const;
    int  getCornerIndexByPosition(const CornerVec& corners,
                                  const Point& pos)const;
protected:
    VoronoiSourceGen srcGen;
};

}

#endif // MAP_POLY_VORONOIGEN_HPP
