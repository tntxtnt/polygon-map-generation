#ifndef MAP_POLY_VORONOISOURCEGEN_HPP
#define MAP_POLY_VORONOISOURCEGEN_HPP

#include <random>
#include <tuple>
#include <optional>
#include "../Point.hpp"
#include "../Rect.hpp"
#include "BoostPolygonRegister.hpp"

namespace map::poly
{

class VoronoiSourceGen
{
public:
    VoronoiSourceGen(unsigned seed=0, Point mapSz={0,0}, unsigned nPoints=1000,
                     unsigned nRelax=2);
    void     relax(unsigned relaxIterCount=1);
    void     pseudorandomPoints();
    void     quasirandomPoints();
    PointVec getSource()const;
    Rect     getBound()const;
    int      getRelaxCount()const;
    bool     isOutside(const Point& p)const;
private:
    void     lloydRelax();
    PointVec getCentroids(const VoronoiDiagram& vd);
    std::tuple<Point,Point>
             getVertices(const VoronoiEdge& edge)const;
    int      clipBoundary(Point& p1, Point& p2)const;
    static std::optional<Point>
             hSegIntersection(Point a, Point b, float y, float x0, float x1);
    static std::optional<Point>
             vSegIntersection(Point a, Point b, float x, float y0, float y1);
private:
    PointVec source;
    Rect     bound;
    unsigned seed;
    unsigned pointCount;
    unsigned relaxCount;
};

}

#endif // MAP_POLY_VORONOISOURCEGEN_HPP
