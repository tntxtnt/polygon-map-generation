#include "Generator.hpp"

namespace map::poly
{

Generator::
Generator(unsigned seed, unsigned nPoints, float w, float h) :
    seed{seed},
    nPoints{nPoints},
    w{w},
    h{h}
{

}

Generator::
~Generator()
{

}

unsigned Generator::
getSeed()const
{
    return seed;
}

float Generator::
getBoundaryWidth()const
{
    return w;
}

float Generator::
getBoundaryHeight()const
{
    return h;
}

unsigned Generator::
getNumPoints()const
{
    return nPoints;
}

}
