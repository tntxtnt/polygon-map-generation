#include "VoronoiSourceGen.hpp"

namespace map::poly
{

VoronoiSourceGen::
VoronoiSourceGen(unsigned seed, Point mapSz, unsigned pointCount,
                 unsigned relaxCount) :
    source    {},
    bound     {0, 0, mapSz.x, mapSz.y},
    seed      {seed},
    pointCount{pointCount},
    relaxCount{relaxCount}
{
    quasirandomPoints();
}

void VoronoiSourceGen::
relax(unsigned relaxIterCount)
{
    while (relaxIterCount--)
    {
        lloydRelax();
        ++relaxCount;
    }
}

void VoronoiSourceGen::
pseudorandomPoints()
{
    std::mt19937 gen{seed};
    std::uniform_real_distribution<float>
        distX(bound.left()+5, bound.right()-10),
        distY(bound.top()+15, bound.bottom()-5);
    source.clear();
    for (unsigned i = 0; i < pointCount; ++i)
        source.emplace_back(distX(gen), distY(gen));
}

void VoronoiSourceGen::
quasirandomPoints()
{
    pseudorandomPoints();
    for (unsigned i = 0; i < relaxCount; ++i)
        lloydRelax();
}

void VoronoiSourceGen::
lloydRelax()
{
    VoronoiDiagram vd;
    construct_voronoi(begin(source), end(source), &vd);
    getCentroids(vd).swap(source);
}

PointVec VoronoiSourceGen::
getCentroids(const VoronoiDiagram& vd)
{
    PointVec centroids;
    for (auto pCell = begin(vd.cells()); pCell != end(vd.cells()); ++pCell)
    {
        Point lastCorner(-1, -1);
        Point centroid(0, 0);
        int nCorners = 0;
        const auto* pEdge = pCell->incident_edge();
        do {
            // get edge vertices (clip infinite edges)
            auto [c1, c2] = getVertices(*pEdge);

            // clip out of bound edges
            if (clipBoundary(c1, c2) >= 0)
            {
                // add corners to centroid
                if (c1 != lastCorner) centroid += c1, nCorners++;
                centroid += c2, nCorners++;
                lastCorner = c2;
            }

            pEdge = pEdge->next();
        } while (pEdge != pCell->incident_edge());

        centroids.push_back(nCorners ? centroid /= nCorners
                                       : source[pCell->source_index()]);
    }
    return centroids;
}

std::tuple<Point,Point> VoronoiSourceGen::
getVertices(const VoronoiEdge& edge)const
{
    Point v[2];
    Point s[2] = { source[edge.cell()->source_index()],
                   source[edge.twin()->cell()->source_index()] };
    if (edge.vertex0())
        v[0] = Point(edge.vertex0()->x(), edge.vertex0()->y());
    if (edge.vertex1())
        v[1] = Point(edge.vertex1()->x(), edge.vertex1()->y());
    if (edge.is_infinite())
    {
        int i = !edge.vertex1(), j = 1 - i;
        Point dir(8.0f*(s[j].y - s[i].y), 8.0f*(s[i].x - s[j].x));
        v[i] = v[j] + dir;
    }
    return {v[0], v[1]};
}

// return -1 if both points are out of bound
// return  0 if both points are inbound
// return  1 if one in one out
int VoronoiSourceGen::
clipBoundary(Point& p1, Point& p2)const
{
    bool p1Out = isOutside(p1), p2Out = isOutside(p2);
    if ( p1Out &&  p2Out) return -1;
    if (!p1Out && !p2Out) return  0;
    Point& pOut = p1Out ? p1 : p2;
    std::optional<Point> ptr;
    if (    (ptr = hSegIntersection(p1, p2, bound.top(),
                                    bound.left(), bound.right())) ||
            (ptr = hSegIntersection(p1, p2, bound.bottom(),
                                    bound.left(), bound.right())) ||
            (ptr = vSegIntersection(p1, p2, bound.left(),
                                    bound.top() , bound.bottom())) ||
            (ptr = vSegIntersection(p1, p2, bound.right(),
                                    bound.top() , bound.bottom()))    )
        pOut = *ptr;
    return 1;
}

bool VoronoiSourceGen::
isOutside(const Point& p)const
{
    return p.x < bound.left() || p.x > bound.right() ||
           p.y < bound.top()  || p.y > bound.bottom();
}

std::optional<Point> VoronoiSourceGen::
hSegIntersection(Point a, Point b, float y, float x0, float x1)
{
    if (b.y < a.y) std::swap(a, b);
    if (a.y > y || b.y < y) return {};
    float x = b.x - (b.y - y) / (b.y - a.y) * (b.x - a.x);
    if (x < x0 || x > x1) return {};
    return Point(x, y);
}

std::optional<Point> VoronoiSourceGen::
vSegIntersection(Point a, Point b, float x, float y0, float y1)
{
    if (b.x < a.x) std::swap(a, b);
    if (a.x > x || b.x < x) return {};
    float y = b.y - (b.x - x) / (b.x - a.x) * (b.y - a.y);
    if (y < y0 || y > y1) return {};
    return Point(x, y);
}


PointVec VoronoiSourceGen::
getSource()const
{
    return source;
}

Rect VoronoiSourceGen::
getBound()const
{
    return bound;
}

int VoronoiSourceGen::
getRelaxCount()const
{
    return relaxCount;
}

}
