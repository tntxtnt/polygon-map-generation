#ifndef MAP_CORNER_HPP
#define MAP_CORNER_HPP

#include "Point.hpp"
#include <vector>
#include <limits>
using IntVec = std::vector<int>;

const double INFTY = std::numeric_limits<double>::infinity();

namespace map
{

struct Corner {
    Point  position{};
    IntVec neighbors{}; // Voronoi diagram's adjacent list
    IntVec polygons{};  // Index of polygons which this corner belongs to
    bool   isBoundary = false;

    bool   isWater    = false;
    bool   isOcean    = false;
    bool   isCoast    = false;
    bool   isShore    = false;
    double elevation  = INFTY;
    double moisture   =  0;
    int    watershed  = -1;
    int    river      =  0;

    void clear();
};

using CornerVec = std::vector<Corner>;

}

#endif // MAP_CORNER_HPP
