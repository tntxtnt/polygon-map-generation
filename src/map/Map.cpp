#include "Map.hpp"

namespace map
{

Map::
Map(const poly::Generator* pPolyGen, const shape::Shape* pMapShape,
    unsigned riverSeed, unsigned riverPickCount) :
    pPolyGen{pPolyGen},
    pMapShape{pMapShape},
    riverSeed{riverSeed},
    riverPickCount{riverPickCount}
{
    rebuild();
}

void Map::
rebuild()
{
    buildPolygonMap();

    assignLandAndWater();
    spreadOceans();
    assignCoastlines();

    assignCornerElevations();
    redistributeElevations();
    assignPolygonElevations();

    createRivers();

    assignCornerMoisture();
    redistributeMoisture();
    assignPolygonMoisture();

    assignBiomes();
}

void Map::
buildPolygonMap()
{
    std::tie(polygons, corners) = pPolyGen->generate();
}

void Map::
assignLandAndWater()
{
    // First decide if each corner of a polygon is water or not,
    //by using MapShape::isWater(cornerPosition)
    // If a polygon has water corners ratio > LAKE_THRESHOLD,
    //mark it water polygon
    // Also mark boundary polygons ocean and water polygons
    const double LAKE_THRESHOLD = 0.34; //more than 1/3
    for (Polygon& polygon : polygons)
    {
        int nWaterCorners = 0;
        for (int cid : polygon.corners)
        {
            corners[cid].isWater = pMapShape->isWater(corners[cid].position);
            if (corners[cid].isWater)
                nWaterCorners++;
        }
        polygon.isWater = nWaterCorners > polygon.corners.size()*LAKE_THRESHOLD;
        if (polygon.isBoundary)
            polygon.isOcean = polygon.isWater = true;
    }
}

void Map::
spreadOceans()
{
    // Collect boundary polygons and mark them ocean
    std::queue<int> oceanPolygons;
    for (int pid = 0; pid < (int)polygons.size(); ++pid)
    {
        if (polygons[pid].isBoundary)
        {
            oceanPolygons.push(pid);
            polygons[pid].isWater = true;
            polygons[pid].isOcean = true;
        }
    }
    // Spread ocean using BFS
    while (!oceanPolygons.empty())
    {
        int pid = oceanPolygons.front(); oceanPolygons.pop();
        for (int qid : polygons[pid].neighbors)
        {
            if (polygons[qid].isWater && !polygons[qid].isOcean)
            {
                polygons[qid].isOcean = true;
                oceanPolygons.push(qid);
            }
        }
    }
}

void Map::
assignCoastlines()
{
    // Make lake corners isWater & land corners !isWater (added)
    for (Corner& corner : corners)
    {
        corner.isWater = false;
        for (int pid : corner.polygons) if (polygons[pid].isWater)
        {
            corner.isWater = true;
            break;
        }
    }
    // Coastlines & shorelines
    for (Corner& corner : corners)
    {
        corner.isOcean = false;
        bool hasLandPolygon = false;
        for (int pid : corner.polygons)
        {
            if (!polygons[pid].isWater) hasLandPolygon = true;
            if (polygons[pid].isOcean)  corner.isOcean = true;
        }
        corner.isCoast = hasLandPolygon && corner.isOcean;
        corner.isShore = !corner.isCoast && hasLandPolygon && corner.isWater;
        // Coast polygon
        if (corner.isCoast)
            for (int pid : corner.polygons)
                polygons[pid].isCoast = true;
    }
}

void Map::
assignCornerElevations()
{
    std::priority_queue<WVert,
                        WVertVec,
                        std::greater<WVert>> minPQ;
    for (int cid = 0; cid < (int)corners.size(); ++cid)
    {
        if (corners[cid].isBoundary)
        {
            corners[cid].elevation = 0;
            minPQ.emplace(cid, corners[cid].elevation);
        }
        else corners[cid].elevation = INFTY;
    }
    while (!minPQ.empty())
    {
        auto u = minPQ.top(); minPQ.pop();
        if (corners[u.id].elevation < u.w) continue;
        for (int vid : corners[u.id].neighbors)
        {
            double addedElevation = 1.01;
            if (corners[u.id].isOcean || corners[vid].isOcean)
                addedElevation = 0.01;
            //else if (corners[u.id].isWater || corners[vid].isWater)
            //    addedElevation = 0.26;
            double newElevation = u.w + addedElevation;
            if (newElevation < corners[vid].elevation)
                minPQ.emplace(vid, corners[vid].elevation = newElevation);
        }
    }
}

void Map::
redistributeElevations()
{
    // Land/Lake elevations
    auto wv = inlandCornerElevations();
    std::sort(begin(wv), end(wv)); //sort by w, here w is elevation
    for (int i = 0; i < (int)wv.size(); ++i)
    {
        double y = (double)i / (wv.size() - 1);
        double x = 1 - sqrt(1 - y);
        corners[wv[i].id].elevation = std::min(1.0, 1.05 * x);
    }
    // Ocean/Coast elevations = 0
    for (auto& corner : corners)
        if (corner.isOcean || corner.isCoast)
            corner.elevation = 0;
}

Map::WVertVec Map::
inlandCornerElevations()const
{
    WVertVec wv;
    for (int i = 0; i < (int)corners.size(); ++i)
        if (!corners[i].isOcean && !corners[i].isCoast)
            wv.emplace_back(i, corners[i].elevation);
    return wv;
}

void Map::
assignPolygonElevations()
{
    for (Polygon& polygon : polygons)
    {
        polygon.elevation = 0;
        for (int cid : polygon.corners)
            polygon.elevation += corners[cid].elevation;
        polygon.elevation /= polygon.corners.size();
    }
}

void Map::
createRivers()
{
    int pickCount = riverPickCount;
    std::mt19937 prngRiver;
    prngRiver.seed(riverSeed);
    std::uniform_int_distribution<int> uniform(0, corners.size() - 1);
    avgRiverLen = 0;
    int nRivers = 0;

    while (pickCount--)
    {
        int startOfRiverCornerId = uniform(prngRiver);
        IntVec riverNodes;
        riverNodes.push_back(startOfRiverCornerId);
        Corner* q = &corners[startOfRiverCornerId];
        if (q->isOcean || q->isCoast ||
            q->elevation < 0.3 || q->elevation > 0.9) continue;
        bool isValidRiver = true;
        while (!q->isCoast)
        {
            int downslopeId = *std::min_element(
                begin(q->neighbors), end(q->neighbors),
                [&](int a, int b){
                    return corners[a].elevation < corners[b].elevation;
                });
            if (corners[downslopeId].elevation >= q->elevation)
            {
                //can't go down any further
                isValidRiver = false;
                break;
            }
            riverNodes.push_back(downslopeId);
            // move q to downslope corner
            q = &corners[downslopeId];
        }
        if (isValidRiver)
        {
            int meetCorner = 0;
            for (int r = 0; r < (int)riverNodes.size() - 1; ++r)
            {
                int addedAmount = r + 1;
                if (corners[riverNodes[r]].river > 0) //already has some rivers
                {
                    if (!meetCorner) meetCorner = r + 1;
                    addedAmount = meetCorner;
                }
                corners[riverNodes[r]].river += addedAmount;
                corners[riverNodes[r]].watershed = riverNodes[r+1];
            }
            corners[end(riverNodes)[-1]].river += riverNodes.size();
            avgRiverLen += riverNodes.size();
            nRivers++;
        }
    }
    avgRiverLen /= nRivers;
}

void Map::
assignCornerMoisture()
{
    std::priority_queue<WVert> maxPQ;
    // we want river nodes moisture from 0 to 1 (extend max to 3)
    // so that lake corners have good moisture after redistribution
    double riverStep = 1.0 / avgRiverLen; //or 0.1

    for (int cid = 0; cid < (int)corners.size(); ++cid)
    {
        auto& q = corners[cid];
        if ((q.isWater || q.river > 0) && !q.isOcean)
        {
            q.moisture = q.isWater ? 1.0 : std::min(3.0, riverStep*q.river);
            maxPQ.emplace(cid, q.moisture);
        }
        else q.moisture = 0;
    }

    while (!maxPQ.empty())
    {
        auto u = maxPQ.top(); maxPQ.pop();
        if (corners[u.id].moisture > u.w) continue;
        for (int vid : corners[u.id].neighbors)
        {
            double newMoisture = u.w * 0.9;
            if (newMoisture > corners[vid].moisture)
            {
                corners[vid].moisture = newMoisture;
                maxPQ.emplace(vid, corners[vid].moisture);
            }
        }
    }
    // Salt water
    for (auto& corner : corners)
        if (corner.isOcean || corner.isCoast)
            corner.moisture = 1.0;
}

void Map::
redistributeMoisture()
{
    auto wv = inlandCornerMoisture();
    std::sort(begin(wv), end(wv));
    for (int i = 0; i < (int)wv.size(); ++i)
    {
        double y = (double)i / (wv.size() - 1);
        double x = y;
        corners[wv[i].id].moisture = x;
    }
}

Map::WVertVec Map::
inlandCornerMoisture()const
{
    WVertVec wv;
    for (int i = 0; i < (int)corners.size(); ++i)
        if (!corners[i].isOcean && !corners[i].isCoast && !corners[i].isWater)
            wv.emplace_back(i, corners[i].moisture);
    return wv;
}

void Map::
assignPolygonMoisture()
{
    for (Polygon& p : polygons)
    {
        p.moisture = 0;
        for (int cid : p.corners)
            p.moisture += std::min(1.0, corners[cid].moisture);
        p.moisture /= p.corners.size();
    }
}

void Map::
assignBiomes()
{
    for (Polygon& p : polygons)
        p.biome = getBiome(p);
        /*if      (p.isOcean) p.biome = Biome::Ocean;
        else if (p.isWater) p.biome = Biome::Lake;
        else if (p.isCoast) p.biome = Biome::Beach;
        else                p.biome = Biome::Land;*/
}

Biome Map::
getBiome(const Polygon& p)const
{
    if (p.isOcean)
        return Biome::Ocean;
    else if (p.isCoast)
        return Biome::Beach;
    else if (p.isWater)
    {
        if (p.elevation < 0.1) return Biome::Marsh;
        else if (p.elevation > 0.8) return Biome::Ice;
        else return Biome::Lake;
    }
    else if (p.elevation > 0.8)
    {
        if (p.moisture > 0.50) return Biome::Snow;
        else if (p.moisture > 0.33) return Biome::Tundra;
        else if (p.moisture > 0.16) return Biome::Bare;
        else return Biome::Scorched;
    }
    else if (p.elevation > 0.6)
    {
        if (p.moisture > 0.66) return Biome::Taiga;
        else if (p.moisture > 0.33) return Biome::Shrubland;
        else return Biome::TemperateDesert;
    }
    else if (p.elevation > 0.3)
    {
        if (p.moisture > 0.83) return Biome::TemperateRainForest;
        else if (p.moisture > 0.50) return Biome::TemperateDeciduousForest;
        else if (p.moisture > 0.16) return Biome::Grassland;
        else return Biome::TemperateDesert;
    }
    else
    {
        if (p.moisture > 0.66) return Biome::TropicalRainForest;
        else if (p.moisture > 0.33) return Biome::TropicalSeasonalForest;
        else if (p.moisture > 0.16) return Biome::Grassland;
        else return Biome::SubtropicalDesert;
    }
}

float Map::
width()const
{
    if (!pPolyGen) return 0;
    return pPolyGen->getBoundaryWidth();
}

float Map::
height()const
{
    if (!pPolyGen) return 0;
    return pPolyGen->getBoundaryHeight();
}

}
