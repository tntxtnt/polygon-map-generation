#include "Point.hpp"

namespace map
{

Point::
Point() :
    x{0},
    y{0}
{

}

Point::
Point(float x, float y) :
    x{x},
    y{y}
{

}

bool Point::
operator<(const Point& rhs)const
{
    return x == rhs.x ? y < rhs.y : x < rhs.x;
}

bool Point::
operator<=(const Point& rhs)const
{
    return *this < rhs || *this == rhs;
}

bool Point::
operator>(const Point& rhs)const
{
    return rhs < *this;
}

bool Point::
operator>=(const Point& rhs)const
{
    return *this > rhs || *this == rhs;
}

bool Point::
operator==(const Point& rhs)const
{
    return x == rhs.x && y == rhs.y;
}

bool Point::
operator!=(const Point& rhs)const
{
    return !(*this == rhs);
}

const Point& Point::
operator+=(const Point& rhs)
{
    x += rhs.x;
    y += rhs.y;
    return *this;
}

const Point& Point::
operator-=(const Point& rhs)
{
    x -= rhs.x;
    y -= rhs.y;
    return *this;
}

const Point& Point::
operator*=(const Point& rhs)
{
    x *= rhs.x;
    y *= rhs.y;
    return *this;
}

const Point& Point::
operator/=(const Point& rhs)
{
    x /= rhs.x;
    y /= rhs.y;
    return *this;
}

Point Point::
operator+(const Point& rhs)const
{
    Point ret = *this;
    return ret += rhs;
}

Point Point::
operator-(const Point& rhs)const
{
    Point ret = *this;
    return ret -= rhs;
}

Point Point::
operator*(const Point& rhs)const
{
    Point ret = *this;
    return ret *= rhs;
}

Point Point::
operator/(const Point& rhs)const
{
    Point ret = *this;
    return ret /= rhs;
}

Point Point::
operator-()const
{
    return {-x, -y};
}

const Point& Point::
operator*=(float rhs)
{
    x *= rhs;
    y *= rhs;
    return *this;
}

const Point& Point::
operator/=(float rhs)
{
    x /= rhs;
    y /= rhs;
    return *this;
}

Point Point::
operator*(float rhs)const
{
    Point ret = *this;
    return ret *= rhs;
}

Point Point::
operator/(float rhs)const
{
    Point ret = *this;
    return ret /= rhs;
}

Point operator*(float lhs, const Point& rhs)
{
    return rhs * lhs;
}

float Point::
angleTo(const Point& b)const
{
    const Point& a = *this;
    Point diff = b - a;
    return atan(diff.y / diff.x) / M_PI * 180;
}

float Point::
distanceTo(const Point& b)const
{
    const Point& a = *this;
    Point diff = b - a;
    return sqrt(diff.x*diff.x + diff.y*diff.y);
}

}
