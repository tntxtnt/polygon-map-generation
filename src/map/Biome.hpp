#ifndef MAP_BIOME_HPP
#define MAP_BIOME_HPP

#include <fmt/ostream.h>

namespace map
{

enum class Biome {
    Land, Water, Ocean, Coast, Lake, Shore, River, Border,
    Marsh, Ice, Beach, Snow, Tundra, Bare, Scorched, Taiga, Shrubland,
    TemperateDesert, TemperateRainForest, TemperateDeciduousForest,
    Grassland, TropicalRainForest, TropicalSeasonalForest,
    SubtropicalDesert,
    COUNT
};

}

std::ostream& operator<<(std::ostream& out, map::Biome b);

#endif // MAP_BIOME_HPP
