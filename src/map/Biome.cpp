#include "Biome.hpp"

std::ostream& operator<<(std::ostream& out, map::Biome b)
{
    const char* s = "";
    switch (b)
    {
    case map::Biome::Land:
        s = "Land";
        break;
    case map::Biome::Water:
        s = "Water";
        break;
    case map::Biome::Ocean:
        s = "Ocean";
        break;
    case map::Biome::Coast:
        s = "Coast";
        break;
    case map::Biome::Lake:
        s = "Lake";
        break;
    case map::Biome::Shore:
        s = "Shore";
        break;
    case map::Biome::River:
        s = "River";
        break;
    case map::Biome::Border:
        s = "Border";
        break;
    case map::Biome::Marsh:
        s = "Marsh";
        break;
    case map::Biome::Ice:
        s = "Ice";
        break;
    case map::Biome::Beach:
        s = "Beach";
        break;
    case map::Biome::Snow:
        s = "Snow";
        break;
    case map::Biome::Tundra:
        s = "Tundra";
        break;
    case map::Biome::Bare:
        s = "Bare";
        break;
    case map::Biome::Scorched:
        s = "Scorched";
        break;
    case map::Biome::Taiga:
        s = "Taiga";
        break;
    case map::Biome::Shrubland:
        s = "Shrubland";
        break;
    case map::Biome::TemperateDesert:
        s = "Temperate Desert";
        break;
    case map::Biome::TemperateRainForest:
        s = "Temperate Rain Forest";
        break;
    case map::Biome::TemperateDeciduousForest:
        s = "Temperate Deciduous Forest";
        break;
    case map::Biome::Grassland:
        s = "Grassland";
        break;
    case map::Biome::TropicalRainForest:
        s = "Tropical Rain Forest";
        break;
    case map::Biome::TropicalSeasonalForest:
        s = "Tropical Seasonal Forest";
        break;
    case map::Biome::SubtropicalDesert:
        s = "Subtropical Desert";
        break;
    default:
        break;
    }
    return out << s;
}
