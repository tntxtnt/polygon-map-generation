#include "Polygon.hpp"

namespace map
{
 
void Polygon::
clear()
{
    source.x = source.y = -1;
    corners.clear();
    neighbors.clear();
    isBoundary = false;
    isWater = false;
    isOcean = false;
    isCoast = false;
    elevation = 0;
    moisture = 0;
    biome = Biome::Land;
}

}
