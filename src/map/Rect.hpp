//Almost the same as sf::FloatRect

#ifndef MAP_RECT_HPP
#define MAP_RECT_HPP

#include "Point.hpp"
#include <cmath>
#include <cstdlib>

namespace map
{
    
class Rect
{
public:
    Rect(float x=0, float y=0, float w=0, float h=0);
    float width()const;
    float height()const;
    float left()const;
    float top()const;
    float right()const;
    float bottom()const;
    bool  contains(float x, float y)const;
    bool  contains(const Point& p)const;
    bool  isNearBoundary(float x, float y, float EPSILON=0.01)const;
    bool  isNearBoundary(const Point& p, float EPSILON=0.01)const;
private:
    Point pos; //top left corner
    float w;
    float h;
};

}

#endif // MAP_RECT_HPP
