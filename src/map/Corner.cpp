#include "Corner.hpp"

namespace map
{

void Corner::
clear()
{
    position.x = position.y = -1;
    neighbors.clear();
    isBoundary = false;
    isWater = false;
    isOcean = false;
    isCoast = false;
    isShore = false;
    elevation = INFTY;
    watershed = -1;
    river = 0;
    moisture = 0;
}

}
