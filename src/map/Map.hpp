#ifndef MAP_MAP_HPP
#define MAP_MAP_HPP

#include <queue>
#include <vector>
#include <random>
#include <algorithm>
#include "Polygon.hpp"
#include "Corner.hpp"
#include "shape/Shape.hpp"
#include "poly/Generator.hpp"
#include <functional>

namespace map
{

class Map
{
public:
    PolygonVec polygons{};
    CornerVec  corners{};
    double     avgRiverLen = 1;

    Map(const poly::Generator* pPolyGen, const shape::Shape* pMapShape,
        unsigned riverSeed, unsigned riverPickCount=75);
    Map(const Map&) = default;
    Map& operator=(const Map&) = default;
    void  rebuild();
    float width()const;
    float height()const;
private:
    struct WVert {
        int id;
        double w;
        WVert(int id, double w) : id(id), w(w) {}
        bool operator<(const WVert& rhs)const { return w < rhs.w; }
        bool operator>(const WVert& rhs)const { return w > rhs.w; }
    };
    using WVertVec = std::vector<WVert>;
    void     buildPolygonMap();
    void     assignLandAndWater();
    void     spreadOceans();
    void     assignCoastlines();
    void     assignCornerElevations();
    void     redistributeElevations();
    WVertVec inlandCornerElevations()const;
    void     assignPolygonElevations();
    void     createRivers();
    void     assignCornerMoisture();
    void     redistributeMoisture();
    WVertVec inlandCornerMoisture()const;
    void     assignPolygonMoisture();
    void     assignBiomes();
    Biome    getBiome(const Polygon& p)const;
private:
    const poly::Generator* pPolyGen       = nullptr;
    const shape::Shape*    pMapShape      = nullptr;
    unsigned               riverSeed      = 0;
    unsigned               riverPickCount = 0;
};

}

#endif // MAP_MAP_HPP
