#ifndef MAP_SHAPE_SHAPE_HPP
#define MAP_SHAPE_SHAPE_HPP

namespace map
{
class Point;
}

namespace map::shape
{

class Shape
{
public:
    virtual bool isWater(const Point& p)const = 0;
    virtual ~Shape() = 0;
};

}

#endif // MAP_SHAPE_SHAPE_HPP
