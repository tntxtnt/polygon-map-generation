// C++ implementation of improved Perlin noise by Ken Perlin
// Java implementation reference: https://mrl.nyu.edu/~perlin/noise/
// Modification:
//   - Remove `permutation` array
//   - Add shuffle() method to shuffle & duplicate `p` array

#ifndef MAP_SHAPE_ISLAND_IMPROVEDPERLINNOISE_HPP
#define MAP_SHAPE_ISLAND_IMPROVEDPERLINNOISE_HPP

#include <array>
#include <cmath>
#include <numeric>
#include <algorithm>

namespace map::shape::island
{

class ImprovedPerlinNoise
{
public:
    ImprovedPerlinNoise();
    double fade(double t)const;
    double lerp(double t, double a, double b)const;
    double grad(int hash, double x, double y, double z)const;
    double noise(double x, double y=0, double z=0)const;
    template <class PRNG> void shuffle(PRNG& prng)
    {
        std::iota(begin(p), end(p), 0);
        std::shuffle(begin(p), begin(p)+256, prng);
        std::copy(begin(p), begin(p)+256, begin(p)+256);
    }
private:
    std::array<double,512> p{};
};

}

#endif // MAP_SHAPE_ISLAND_IMPROVEDPERLINNOISE_HPP
