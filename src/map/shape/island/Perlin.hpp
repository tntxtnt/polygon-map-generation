#ifndef MAP_SHAPE_ISLAND_PERLIN_HPP
#define MAP_SHAPE_ISLAND_PERLIN_HPP

#include <fstream>
#include <string>
#include <random>
#include "ImprovedPerlinNoise.hpp"
#include "../Shape.hpp"
#include "../../Point.hpp"

namespace map::shape::island
{

class Perlin : public Shape
{
public:
    Perlin();
    ~Perlin();
    void seed(unsigned n);
    void loadSettings(const std::string& filename);
    void setMapSize(float w, float h);
    bool isWater(const Point& p)const;
private:
    double noise(double nx, double ny)const;
    double height(double x, double y)const;
private:
    ImprovedPerlinNoise perlin{};
    float       w       =  0;
    float       h       =  0;
    unsigned    seedVal =  0;
    double      dx      = -1;
    double      dy      = -1;
    double      emult   =  7;
    double      seaLvl  =  0.175;
    struct OctaveCoef {
        double  a       = 1;
        double  b       = 0.5;
        double  c       = 0.25;
        double  d       = 0.125;
        double  e       = 0.0625;
        double  f       = 0.03125;
        double  power   = 6;

        double sum()const { return a + b + c + d + e + f; }
    } ocf{};
    struct IslandCoef {
        double  a       = 0.02;
        double  b       = 1;
        double  c       = 4;

        double multiply(double e, double d)const
        { return (e + a) * (1 - b*pow(d, c)); }
        double add(double e, double d)const
        { return e + a - b*pow(d, c); }
        double manhattanDistance(double nx, double ny)const
        { return 2 * std::max(fabs(nx), fabs(ny)); }
        double euclideanDistance(double nx, double ny)const
        { return 2 * sqrt(nx*nx + ny*ny); }
    } icf{};
};

}

#endif // MAP_SHAPE_ISLAND_PERLIN_HPP
