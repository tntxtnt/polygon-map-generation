#include "Perlin.hpp"

namespace map::shape::island
{

Perlin::
Perlin()
{

}

Perlin::
~Perlin()
{

}

bool Perlin::
isWater(const Point& p)const
{
    return height((double)p.x, (double)p.y) < seaLvl;
}

double Perlin::
noise(double nx, double ny)const
{
    return perlin.noise(4*nx, 4*ny) / 2.0 + 0.5; //Rescale from -1:1 to 0:1
}

void Perlin::
setMapSize(float w, float h)
{
    this->w = w;
    this->h = h;
}

void Perlin::
seed(unsigned n)
{
    seedVal = n;
    std::mt19937 prng;
    prng.seed(seedVal);
    perlin.shuffle(prng);
}

void Perlin::
loadSettings(const std::string& filename)
{
    std::fstream fin(filename);
    fin >> dx >> dy >> emult >> seaLvl
        >> ocf.a >> ocf.b >> ocf.c >> ocf.d >> ocf.e >> ocf.f >> ocf.power
        >> icf.a >> icf.b >> icf.c;
}

double Perlin::
height(double x, double y)const
{
    float minWH = std::min(w, h);
    double nx = (x/minWH + dx - 0.5);
    double ny = (y/minWH + dy - 0.5);
    double cx = (x/w + dx - 0.5);
    double cy = (y/h + dy - 0.5);
    double e  = ocf.a * noise(  nx,   ny) + ocf.d * noise( 8*nx,  8*ny) +
                ocf.b * noise(2*nx, 2*ny) + ocf.e * noise(16*nx, 16*ny) +
                ocf.c * noise(4*nx, 4*ny) + ocf.f * noise(32*nx, 32*ny);
    e /= ocf.sum();
    e = pow(e, ocf.power);
    double d = icf.euclideanDistance(cx - dx, cy - dy);
    e = icf.multiply(e, d);
    e *= emult;
    if (e > 1) e = 1;
    if (e < 0) e = 0;
    return e;
}

}
