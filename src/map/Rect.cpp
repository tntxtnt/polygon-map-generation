#include "Rect.hpp"

namespace map
{

Rect::
Rect(float x, float y, float w, float h) :
    pos{x, y},
    w{w},
    h{h}
{

}


float Rect::
width()const
{
    return w;
}

float Rect::
height()const
{
    return h;
}

float Rect::
left()const
{
    return pos.x;
}

float Rect::
top()const
{
    return pos.y;
}

float Rect::
right()const
{
    return pos.x + w;
}

float Rect::
bottom()const
{
    return pos.y + h;
}

bool Rect::
contains(float x, float y)const
{
    return left() <= x&&x < right() &&
           top()  <= y&&y < bottom();
}

bool Rect::
contains(const Point& p)const
{
    return contains(p.x, p.y);
}

bool Rect::
isNearBoundary(float x, float y, float EPSILON)const
{
    return fabs(x - left()  ) < EPSILON ||
           fabs(x - right() ) < EPSILON ||
           fabs(y - top()   ) < EPSILON ||
           fabs(y - bottom()) < EPSILON;
}

bool Rect::
isNearBoundary(const Point& p, float EPSILON)const
{
    return isNearBoundary(p.x, p.y, EPSILON);
}

}
