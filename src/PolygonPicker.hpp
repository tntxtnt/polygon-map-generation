#ifndef POLYGONPICKER_HPP
#define POLYGONPICKER_HPP

#include <SFML/Graphics.hpp>
#include <vector>
#include <algorithm>

namespace map
{
class Map;
class Corner;
class Polygon;
}
using IntVec        = std::vector<int>;
using PidIncList    = IntVec;
using PidIncListVec = std::vector<PidIncList>;
using PidIncListMat = std::vector<PidIncListVec>;

class PolygonPicker
{
public:
    PolygonPicker(unsigned rows, unsigned cols, float mapX, float mapY,
                  float mapW, float mapH);
    void parse(const map::Map& map);
    int  getPickedPid(float tx, float ty, const map::Map& Map)const;
private:
    IntVec       getPossiblePids(float tx, float ty)const;
    sf::Vector2i getCornerRowCol(const map::Corner& corner);
    static bool  isInside(float rx, float ry, int pid, const map::Map& map);
private:
    PidIncListMat pidMap;
    sf::FloatRect pickRegion;
    float         cellW;
    float         cellH;
};

#endif // POLYGONPICKER_HPP
