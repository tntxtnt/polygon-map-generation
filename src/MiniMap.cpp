#include "MiniMap.hpp"

void MiniMap::
setViewBorder(float thickness, sf::Color color)
{
    borderThickness = thickness;
    viewBorder.setFillColor(sf::Color::Transparent);
    viewBorder.setOutlineThickness(thickness);
    viewBorder.setOutlineColor(color);
}

void MiniMap::
setBigMapViewRect(const sf::FloatRect* pRect)
{
    pBigMapViewRect = pRect;
}

const sf::RectangleShape& MiniMap::
getSyncViewBorder()
{
    viewBorder.setSize({pBigMapViewRect->width - borderThickness,
                        pBigMapViewRect->height - borderThickness});
    viewBorder.setPosition(pBigMapViewRect->left + borderThickness/2,
                           pBigMapViewRect->top  + borderThickness/2);
    return viewBorder;
}
