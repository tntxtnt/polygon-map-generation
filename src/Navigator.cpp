#include "Navigator.hpp"

Navigator::
Navigator(sf::FloatRect src, sf::FloatRect dest, sf::Vector2u windowSz) :
    dstRect{dest},
    srcRect{src},
    viewRect{srcRect},
    view{viewRect},
    mousePressedPos{-1, -1},
    srcToDstRatio{srcRect.width/dstRect.width, srcRect.height/dstRect.height},
    zoomMultiplier{1.25f}
{
    view.setViewport({dstRect.left/windowSz.x, dstRect.top/windowSz.y,
                      dstRect.width/windowSz.x, dstRect.height/windowSz.y});
}

Navigator::
~Navigator()
{

}

const sf::View& Navigator::
getView()const
{
    return view;
}

float Navigator::
getZoomPercentage()const
{
    return srcRect.width / view.getSize().x;
}

const sf::FloatRect& Navigator::
getCapturedRect()const
{
    return viewRect;
}

float Navigator::
viewPercentage()const
{
    return view.getSize().x / srcRect.width;
}

void Navigator::
reset()
{
    viewRect = srcRect;
    view.reset(viewRect);
}

void Navigator::
setZoomMultiplier(float f)
{
    zoomMultiplier = f;
}

float Navigator::
getZoomMultiplier()const
{
    return zoomMultiplier;
}

void Navigator::
setSourceRect(sf::FloatRect src)
{
    srcRect = src;
    srcToDstRatio.x = srcRect.width / dstRect.width;
    srcToDstRatio.y = srcRect.height / dstRect.height;
    reset();
}

sf::FloatRect Navigator::
getSourceRect()const
{
    return srcRect;
}

void Navigator::
setDestinationRect(sf::FloatRect dst, sf::Vector2u windowSz)
{
    dstRect = dst;
    srcToDstRatio.x = srcRect.width / dstRect.width;
    srcToDstRatio.y = srcRect.height / dstRect.height;
    view.setViewport({dstRect.left/windowSz.x, dstRect.top/windowSz.y,
                      dstRect.width/windowSz.x, dstRect.height/windowSz.y});
}

sf::FloatRect Navigator::
getDestinationRect()const
{
    return dstRect;
}

void Navigator::
handleEvent(const sf::Event& e)
{
    switch (e.type)
    {
    case sf::Event::MouseButtonPressed:
        if (e.mouseButton.button == sf::Mouse::Left)
            handleLeftMousePressed(e.mouseButton.x, e.mouseButton.y);
        break;
    case sf::Event::MouseButtonReleased:
        if (e.mouseButton.button == sf::Mouse::Left)
            handleLeftMouseReleased();
        else if (e.mouseButton.button == sf::Mouse::Middle)
            reset();
        break;
    case sf::Event::MouseMoved:
        handleMouseMoved(e.mouseMove.x, e.mouseMove.y);
        break;
    case sf::Event::MouseWheelScrolled:
        if (e.mouseWheelScroll.wheel == sf::Mouse::VerticalWheel)
        {
            auto const& mws = e.mouseWheelScroll;
            float s = mws.delta > 0 ? 1 / zoomMultiplier : zoomMultiplier;
            for (int repeat = abs(mws.delta); repeat--; )
                zoomAtViewportPoint(mws.x, mws.y, s);
        }
        break;
    default:
        break;
    }
}

void Navigator::
zoomAtViewportPoint(float x, float y, float percentage)
{
    if (!isInsideEventRect(x, y)) return;
    x -= dstRect.left;
    y -= dstRect.top;
    float zp = viewPercentage();
    sf::Vector2f viewPos = view.getCenter() - 0.5f * view.getSize();
    viewRect.left   = (1 - percentage)*x*zp*srcToDstRatio.x + viewPos.x;
    viewRect.top    = (1 - percentage)*y*zp*srcToDstRatio.y + viewPos.y;
    viewRect.width  = percentage * view.getSize().x;
    viewRect.height = percentage * view.getSize().y;
    view.reset(viewRect);
}

void Navigator::
handleLeftMousePressed(float x, float y)
{
    mousePressedPos.x = x;
    mousePressedPos.y = y;
    if (!isInsideEventRect(x, y))
        mousePressedPos.x = mousePressedPos.y = -1;
}

void Navigator::
handleLeftMouseReleased()
{
    mousePressedPos.x = mousePressedPos.y = -1;
}

void Navigator::
handleMouseMoved(float x, float y)
{
    if (mousePressedPos.x < 0) return;
    sf::Vector2f mousePos{x, y};
    auto moveAmount = (mousePressedPos - mousePos) * viewPercentage();
    moveAmount.x *= srcToDstRatio.x;
    moveAmount.y *= srcToDstRatio.y;
    viewRect.left += moveAmount.x;
    viewRect.top += moveAmount.y;
    view.reset(viewRect);
    mousePressedPos = mousePos;
    if (!isInsideEventRect(mousePressedPos.x, mousePressedPos.y))
        mousePressedPos.x = mousePressedPos.y = -1;
}

sf::Vector2f Navigator::
getTransformedMousePos(float mx, float my)const
{
    float z = viewPercentage();
    float x = mx - dstRect.left;
    float y = my - dstRect.top;
    x = x*z + dstRect.left + viewRect.left;
    y = y*z + dstRect.top  + viewRect.top;
    return {x, y};
}

bool Navigator::
isInsideEventRect(float mx, float my)const
{
    return dstRect.contains(mx, my);
}
