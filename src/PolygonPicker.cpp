#include "PolygonPicker.hpp"
#include "map/Map.hpp"

PolygonPicker::
PolygonPicker(unsigned rows, unsigned cols, float mapX, float mapY,
              float mapW, float mapH) :
    pidMap(rows, PidIncListVec(cols)),
    pickRegion{mapX, mapY, mapW, mapH},
    cellW{mapW / cols},
    cellH{mapH / rows}
{

}

void PolygonPicker::
parse(const map::Map& map)
{
    for (size_t i = 0; i < pidMap.size(); ++i)
        for (size_t j = 0; j < pidMap[i].size(); ++j)
            pidMap[i][j].clear();
    for (const auto& corner : map.corners)
    {
        auto p = getCornerRowCol(corner);
        for (int pid : corner.polygons)
        {
            auto& pids = pidMap[p.y][p.x];
            if (std::find(begin(pids), end(pids), pid) == end(pids))
                pids.push_back(pid);
        }
    }
}

sf::Vector2i PolygonPicker::
getCornerRowCol(const map::Corner& corner)
{
    int col = corner.position.x / cellW;
    if (col >= (int)pidMap[0].size()) col = pidMap[0].size() - 1;
    int row = corner.position.y / cellH;
    if (row >= (int)pidMap.size()) row = pidMap.size() - 1;
    return sf::Vector2i{col, row};
}

IntVec PolygonPicker::
getPossiblePids(float tx, float ty)const
{
    if (!pickRegion.contains(tx, ty)) return IntVec{};
    float x = tx - pickRegion.left;
    float y = ty - pickRegion.top;
    int row = y / cellH;
    int col = x / cellW;
    return pidMap[row][col];
}

int PolygonPicker::
getPickedPid(float tx, float ty, const map::Map& map)const
{
    float x = tx - pickRegion.left;
    float y = ty - pickRegion.top;
    for (int pid : getPossiblePids(tx, ty))
        if (isInside(x, y, pid, map))
            return pid;
    return -1;
}

bool PolygonPicker::
isInside(float x, float y, int pid, const map::Map& map)
{
    int pos = 0;
    int neg = 0;
    auto const& polygon = map.polygons[pid];
    int nCorners = polygon.corners.size();
    for (int i = 0; i < nCorners; ++i)
    {
        int a = polygon.corners[i];
        int b = polygon.corners[(i+1)%nCorners];

        //Compute the cross product
        float x1 = map.corners[a].position.x;
        float y1 = map.corners[a].position.y;
        float x2 = map.corners[b].position.x;
        float y2 = map.corners[b].position.y;
        float d = (x - x1)*(y2 - y1) - (y - y1)*(x2 - x1);

        if (d == 0) return true;
        if (d > 0) pos++; else neg++;
        if (pos > 0 && neg > 0) return false;
    }
    return true;
}
