#ifndef NAVIGATOR_HPP
#define NAVIGATOR_HPP

#include <SFML/Graphics.hpp>

/// Navigator: works like a projector, project a region to another fixed region
///            can handle dragging/zooming mouse event in the projected region.
/// You may draw your things at [(0, 0), (600, 600)] and easily project it onto 
/// [(100, 100), (500, 500)] by calling `window.setView(nav.getView())` before 
/// drawing.
/// If you don't need to project anything and only need navigation feature,
/// just don't call `window.setView(nav.getView())`.
/**
 @srcRect the default view rect when reset() is called
 @dstRect the projected area on render window
 @viewRect the internal FloatRect of view
 @view the real sf::View
 @mousePressedPos record previous mouse down position to help with dragging
 @srcToDstRatio store srcRect/dstRect ratio to correct dragging/zooming behavior
 @zoomMultiplier zoom multiplier. value > 1.0f: forward scrolling = zoom in
                                  value < 1.0f: backward scrolling = zoom in
*/
class Navigator
{
public:
    /// Ctor
    /// @param src The area to capture. Processed events may move/zoom
    /// @param dest The area to project captured area onto.
    ///Events such as mouse events are captured only in this area.
    /// @param windowSz Screen size. This helps converting `dest`
    ///to a viewport rect which width and height are between 0 and 1.
    Navigator(sf::FloatRect src, sf::FloatRect dest, sf::Vector2u windowSz);

    virtual ~Navigator();

    /// Get the projected view. Call `window.setView(nav.getView());` before
    ///drawing the captured area.
    const sf::View& getView()const;

    /// Get current captured area rect.
    const sf::FloatRect& getCapturedRect()const;

    /// Get current zooming percentage (1.0f is 100% zoom)
    float getZoomPercentage()const;

    sf::Vector2f getTransformedMousePos(float mx, float my)const;

    /// Original `src` rect. This is not the current captured rect, use
    ///`getCapturedRect()` if you want to get it instead.
    sf::FloatRect getSourceRect()const;

    /// Original `dest` rect.
    sf::FloatRect getDestinationRect()const;

    /// Get multiplier constant per zoom. This is not the current 
    ///zooming percentage, use `getZoomPercentage()` if you want to
    //get it instead.
    float getZoomMultiplier()const;

    /// Set original `src` rect. This will also reset current captured
    ///rect. Beware: it may change aspect ratio of projected area.
    void setSourceRect(sf::FloatRect src);

    /// Set original `dest` rect. Beware: it may change aspect ratio
    //of projected area.
    void setDestinationRect(sf::FloatRect, sf::Vector2u);

    /// Set zoom multiplier constant per zoom.
    ///   f > 1.0f: forward scrolling => zoom in
    ///   f < 1.0f: forward scrolling => zoom out
    void setZoomMultiplier(float f);

    /// Check if mouse is inside event rect (which is `dest` rect)
    bool isInsideEventRect(float mx, float my)const;

    /// Handle events: zooming using mouse scrolling, moving using
    ///mouse dragging, restore to default zoom using mid click.
    void handleEvent(const sf::Event& e);

private:
    void  zoomAtViewportPoint(float x, float y, float percentage);
    void  reset();
    float viewPercentage()const;
    void  handleLeftMousePressed(float x, float y);
    void  handleLeftMouseReleased();
    void  handleMouseMoved(float x, float y);
protected:
    sf::FloatRect dstRect;
    sf::FloatRect srcRect;
    sf::FloatRect viewRect;
    sf::View      view;
    sf::Vector2f  mousePressedPos;
    sf::Vector2f  srcToDstRatio;
    float         zoomMultiplier;
};

#endif // NAVIGATOR_HPP
