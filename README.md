# Polygon Map Generation

A strictly 2D polygon map generation.

C++ adaptation based on Amit Patel's [Polygonal Map Generation for Games](http://www-cs-students.stanford.edu/~amitp/game-programming/polygon-map-generation/)

## License
MIT

## Dependencies

- [Boost](http://www.boost.org/) (Boost.Polygon)
- [SFML](https://www.sfml-dev.org/index.php) 2.4.2 (for Drawing)
- [fmt](https://github.com/fmtlib/fmt)

## Screenshot
![screen](image/screen.png)

## Samples

![Sample 0](image/s0.png)

![Sample 1](image/s1.png)

![Sample 2](image/s2.png)

![Sample 3](image/s3.png)

![Sample 3](image/s4.png)


## Building Steps
### Build Polygon Map

- Produce `n` random 2D points called `source` 
- Lloyd relaxation: Build Voronoi graph `vd` from `source`. For each cell in `vd`, calculate its centroid. Collect all `centroids` of `vd` and swap them with original `source`.
 
![Lloyd relaxation](image/1.gif)

- Repeat relaxation until satisfy.
- Ignore boundary cells. This is purely because of Boost.Polygon does not offer clipping edges, and my edge clipping algorithm is buggy, so I decided to ignore the boundary cells, and mark the inner corners of these boundary cells as boundary corners.

![Ignore boundary cells](image/2.png)

- Voronoi cells can have edges with very small length edge lengths, or corners that are too close to each other, no matter how many relaxations. For example, take a closer look at the top right of the above image:

![Ugly edge](image/3.png)

- These small edges will make the map look ugly, so we take one more step to improve the corners with `improveCorners()` function in `ImproveVoroGen` class. A corner will be assign to new position that is the average sources of all polygons it touches. The result map is much better, but it loses properties of a Voronoi diagram. I think it's fine since we don't need those Voronoi properties here.

![Improve corners](image/4.png)

- The final polygon map will be a dual graph that represent both Delaunay triangulation (in red, with `polygon.neighbors`) and Voronoi diagram (in green, with `corner.neighbors`). This map has about 200 polygons.

![Final map](image/5.png)

### Assign Land And Water

- From [Making maps with noise functions](http://www.redblobgames.com/maps/terrain-from-noise/) and [Improved Noise reference implementation](https://mrl.nyu.edu/~perlin/noise/), I made a map shaping class that can tell whether a point is land (or water) and form an island shape. This is the result shape (seed 1500000000) I ran on every pixel:

![Map shaping](image/6.png)

- Combine it with polygon map (~20000 polygons after 2 relaxations), just taking water/land of corners:

![Island land](image/7.png)

### Spread Ocean

- Start withs all boundary polygons as ocean polygon, spread to all connected water polygon. The remaining untouched water polygons will become lakes.

![Spread ocean](image/8.png)

### Assign Coastlines

- For each corner, if it touches both **ocean** polygon and land polygon, then mark it as coast corner. Land polygons that have one or more coast corners will also be marked as coast polygons.

![Assign coastlines](image/9.png)

### Assign Elevations

- Form a priority queue of all boundary corners, perform Dijkstra's algorithm to all other corners. An `u-v` edge has weight of `0.01` if either `u` or `v` is ocean corner, and `1.01` otherwise. These magic numbers can change depends on your needs. You can even add an additional random weight to it.

- Redistribute elevations: sort all corners by their elevations, so each corner will have a float value `y` valued `0` to `1` corresponding to its position in the sorted corners.

![Assign Elevations](image/10.png)

- Since we don't want our island to be a rocky island, we can map `y` value to new value `x` so there will be fewer mountainous polygons. The function we're using is `x = 1 - sqrt(1 - y)`.

![Assign Elevations](image/11.png)

- Finally assign polygon evlevations as average elevation of all of its corners. The result (whiter = higher elevations). You can see the difference before and after mapping `y` value (whiter) to `x` value.


### Create Rivers

- Pick a random land corner `q` with desired elevation (0.3 .. 0.9) as river starting node. Pick next node (watershed) by finding the node with lowest elevation in `q.neighbors`. Assign `q` to next node and repeat the process until `q` is a coast corner. If by any chance next node's elevation is higher than current node's elevation, then we ignore the whole river.
- Larger number of picks obviously means larger number of rivers. I use `pickCount = 50` 

![Create rivers](image/12.png)

- In my implementation rivers can go through lakes.

### Assign Moisture

- Same process as assign elevations, but we start from a priority queue of water corners, with lake/coast corners start with moisture `1.0`, and river corners from `1.0` to `3.0`. Each edge jump we decrease moisture value by 10%.
- Redistribute moisture: same process as redistributing elevations, but the function we use here is `y = x` (or `y` value is good enough). Here is the result (whiter = dryer)

![Assign moisture](image/13.png)

### Assign Biomes

- Final step: combine elevations and moisture to make biomes. You can read the details in [Amit's original article](http://www-cs-students.stanford.edu/~amitp/game-programming/polygon-map-generation/#biomes).

![Final result](image/14.png)

## Other Features

Noisy edges, lava, road, other kinds of polygon map, other people's approaches, more at [original article](http://www-cs-students.stanford.edu/~amitp/game-programming/polygon-map-generation/)
