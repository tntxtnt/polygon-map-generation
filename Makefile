TGTNAME=pmg

CXX=g++ -std=c++1z
RM=rm -rf
MKDIR=mkdir -p
ECHO=echo
rwildcard=$(wildcard $1$2) $(foreach d,$(wildcard $1*),$(call rwildcard,$d/,$2))

INCDIR=-Isrc
LNKDIR=

CFLAGS=-O2 -Wall
LFLAGS=-s -lsfml-graphics -lsfml-window -lsfml-system -lfmt -pthread -lopengl32

SRCDIR=src
SRCS=$(call rwildcard,$(SRCDIR),*.cpp)
OBJDIR=obj
OBJS=$(SRCS:$(SRCDIR)/%.cpp=$(OBJDIR)/%.o)
DEPDIR=dep
DEPS=$(OBJS:$(OBJDIR)/%.o=$(DEPDIR)/%.dep)
OBJDIRS=$(sort $(dir $(OBJS)))
DEPDIRS=$(OBJDIRS:$(OBJDIR)/%=$(DEPDIR)/%)
TGTDIR=bin
TARGET=$(TGTDIR)/$(TGTNAME)

print-%  : ; @echo $* = $($*)

.PHONY: all run clean

all: $(TARGET)

run: $(TARGET)
	./$(TGTDIR)/$(TGTNAME)
	
clean:
	@$(RM) $(OBJS)
	@$(RM) $(DEPS)
	@$(RM) $(TGTDIR)/$(TGTNAME)
	
$(OBJDIRS):
	@$(MKDIR) $@
$(DEPDIRS):
	@$(MKDIR) $@
$(TGTDIR):
	@$(MKDIR) $@

$(DEPDIR)/%.dep: $(SRCDIR)/%.cpp | $(DEPDIRS)
	$(eval OBJDEP=$(shell $(CXX) $(INCDIR) -MM $< -MT $(<:$(SRCDIR)/%.cpp=$(OBJDIR)/%.o)))
	$(eval DEPDEP=$(subst .o:,.dep:,$(OBJDEP:$(OBJDIR)/%=$(DEPDIR)/%)))
	@$(ECHO) $(OBJDEP) > $@
	@$(ECHO) $(DEPDEP) >> $@
-include $(DEPS)

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp | $(OBJDIRS)
	$(CXX) -c $(INCDIR) $(CFLAGS) $< -o $@
	
$(TARGET): $(OBJS) | $(TGTDIR)
	$(CXX) $^ $(LNKDIR) $(LFLAGS) -o $(TARGET)
